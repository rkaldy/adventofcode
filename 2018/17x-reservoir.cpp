#include "lib/codejam.h"
#include "lib/point.h"
#include "lib/png.h"

using namespace std;

char b[2000][2000];
int xmin = INT_MAX, xmax = 0, ymin = INT_MAX, ymax = 0;


void print() {
#ifdef LOGGING
	forr(y, ymax+1) {
		forrr(x, xmin, xmax+1) {
			cout << b[y][x];
		}
		cout << endl;
	}
	cout << endl;
#endif
}


bool step(int x, int y) {
	if (y > ymax || b[y][x] == '|') {
		return true;
	} else if (b[y][x] != '.') {
		return false;
	}
	b[y][x] = '*';
	print();

	bool flow_down, flow_left, flow_right;
	flow_down = step(x, y+1);
	if (!flow_down) {
		flow_left = step(x-1, y);
		flow_right = step(x+1, y);
		
	}
	if (flow_left && !flow_right) {
		for (int sx = x+1; b[y][sx] == '~'; sx++) b[y][sx] = '|';
	}
	if (!flow_left && flow_right) {
		for (int sx = x-1; b[y][sx] == '~'; sx--) b[y][sx] = '|';
	}

	bool flow = flow_down | flow_left | flow_right;
	b[y][x] = flow ? '|' : '~';
	print();
	return flow;
}


int main() {
	string line;	
	int xfrom, xto, yfrom, yto;
	memset(b, '.', sizeof(b));
	while (getline(cin, line)) {
		if (line[0] == 'x') {
			sscanf(line.c_str(), "x=%i, y=%i..%i", &xfrom, &yfrom, &yto);
			forrr(y, yfrom, yto+1) {
				b[y][xfrom] = '#';
				if (xfrom > xmax) xmax = xfrom;
				if (xfrom < xmin) xmin = xfrom;
				if (y < ymin) ymin = y;
				if (y > ymax) ymax = y;
			}
		} else {
			sscanf(line.c_str(), "y=%i, x=%i..%i", &yfrom, &xfrom, &xto);
			forrr(x, xfrom, xto) {
				b[yfrom][x] = '#';
				if (x > xmax) xmax = x;
				if (x < xmin) xmin = x;
				if (yfrom < ymin) ymin = yfrom;
				if (yfrom > ymax) ymax = yfrom;
			}
		}
	}
	xmin--; xmax++;

	step(500, 0);
	visualize(b, "17.png", {{'.', 255, 255, 255}, {'~', 64, 64, 255}, {'|', 0, 224, 255}, {'#', 40, 40, 40}}, 3, xmin, xmax, ymin, ymax);
	
	int reach = 0;
	int reach_dry = 0;
	forrr(y, ymin, ymax+1) {
		forrr(x, xmin, xmax+1) {
			if (b[y][x] == '~' || b[y][x] == '|') reach++; 
			if (b[y][x] == '~') reach_dry++;
		}
	}
	cout << reach << endl << reach_dry << endl;

	return 0;
}
