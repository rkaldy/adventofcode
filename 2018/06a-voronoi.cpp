#include "lib/codejam.h"
#include "lib/geometry.h"
#include "lib/minimize.h"

using namespace std;

int main() {
	vector<ipoint> ps;
	string line;
	while (getline(cin, line)) {
		int x, y;
		sscanf(line.c_str(), "%i, %i", &x, &y);
		ps.emplace_back(x, y);
	}

	int xmin = min(ps, &ipoint::x), xmax = max(ps, &ipoint::x);
	int ymin = min(ps, &ipoint::y), ymax = max(ps, &ipoint::y);
	
	set<int> ch = convex_hull_set(ps);

	vector<int> area(ps.size(), 0);
	vector<int> nearest;
	for (int y = ymin; y <= ymax; y++) {
		for (int x = xmin; x <= xmax; x++) {
			ipoint q(x, y);
			int dist = 0;
			minimize_multi(ps.size(), nearest, [q, &ps](int i) { 
				return abs(ps[i].x - q.x) + abs(ps[i].y - q.y);
			});
			if (nearest.size() == 1 && !has(ch, nearest[0])) {
				area[nearest[0]]++;
			}
		}
	}

	LOG(PAR(ps));
	LOG(PAR(ch));
	LOG(PAR(area));
	cout << max(area) << endl;
}
