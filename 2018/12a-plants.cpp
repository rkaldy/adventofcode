#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

#ifdef NDEBUG
#define BSIZE 300
#define BOFF 50
#else
#define BSIZE 40
#define BOFF 3
#endif

bool b[BSIZE], b2[BSIZE];
bool t[32];

void print() {
#ifdef LOGGING
	forr (i, BSIZE) {
		cerr << (b[i] ? '#' : '.');
	}
	cerr << endl;
#endif
}


int main() {
	string ln;
	getline(cin, ln);
	forrr (i, 15, ln.size()) {
		b[i-15+BOFF] = ln[i] == '#';
	}
	while (getline(cin, ln)) {
		int f = 0;
		forr (i, 5) {
			f <<= 1;
			if (ln[i] == '#') f += 1;
		}
		t[f] = ln[9] == '#';
	}

	print();
	forr (g, 20) {
		zero(b2);
		int f = 0;
		forr (i, 5) {
			f <<= 1;
			f += b[i];
		}
		forrr (i, 2, BSIZE-2) {
			b2[i] = t[f];
			f <<= 1;
			f &= 31;
			if (i < BSIZE-3) f += b[i+3];
		}
		memcpy(b, b2, BSIZE*sizeof(bool));
		print();
	}

	int ret = 0;
	forr (i, BSIZE) {
		if (b[i]) ret += i-BOFF;
	}
	cout << ret << endl;

	return 0;
}
