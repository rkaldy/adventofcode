#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;


int tx, ty, depth;
int b[1000][1000];


int el(int gi) { return (gi + depth) % 20183 % 3; }

void print() {
#ifdef LOGGING
	forr (y, ty+1) {
		forr (x, tx+1) {
			switch (el(b[y][x])) {
				case 0: cout << '.'; break;
				case 1: cout << '='; break;
				case 2: cout << '|'; break;
			}
		}
		cout << endl;
	}
	cout << endl;
#endif
}


int main() {

	scanf("depth: %i\n", &depth);
	scanf("target: %i,%i\n", &tx, &ty);

	b[0][0] = 0;
	forrr (x, 1, tx+1) b[0][x] = (x * 16807) % 20183;
	forrr (y, 1, ty+1) b[y][0] = (y * 48271) % 20183;
	forrr (y, 1, ty+1) {
		forrr (x, 1, tx+1) {
			b[y][x] = ((b[y-1][x] + depth) * (b[y][x-1] + depth)) % 20183;
		}
	}
	print();

	int risk = 0;
	forr (y, ty+1) forr (x, tx+1) risk += el(b[y][x]);
	risk -= el(b[ty][tx]);
	cout << risk << endl;

	return 0;
}
