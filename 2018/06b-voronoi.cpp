#include "lib/codejam.h"
#include "lib/geometry.h"

using namespace std;

int main() {
	vector<ipoint> ps;
	string line;
	while (getline(cin, line)) {
		int x, y;
		sscanf(line.c_str(), "%i, %i", &x, &y);
		ps.emplace_back(x, y);
	}

	int xmin = INT_MAX, xmax = 0;
	int ymin = INT_MAX, ymax = 0;
	for (ipoint& p : ps) {
		if (p.x < xmin) xmin = p.x;
		if (p.x > xmax) xmax = p.x;
		if (p.y < ymin) ymin = p.y;
		if (p.y > ymax) ymax = p.y;
	}
	LOG(PAR(ps));

	int area = 0;
	for (int y = ymin; y <= ymax; y++) {
		for (int x = xmin; x <= xmax; x++) {
			ipoint q(x, y);
			int dist = 0;
			for (auto& p : ps) {
				dist += abs(p.x - q.x) + abs(p.y - q.y);
				if (dist >= 10000) break;
			}
			if (dist < 10000) area++;
		}
	}
	cout << area << endl;
}
