#!/usr/bin/python3

import sys, networkx

g = networkx.DiGraph()
for line in sys.stdin:
    line = line.split(" ")
    a = line[1]
    b = line[7]
    g.add_edge(a, b)

res = list(networkx.algorithms.dag.lexicographical_topological_sort(g))
print("".join(res))
