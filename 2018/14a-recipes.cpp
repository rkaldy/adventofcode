#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

int n, a, b;
vector<int> r;

void print() {
#ifdef LOGGING
	forr (i, r.size()) {
		if (i == a) {
			cout << "(" << r[i] << ")";
		} else if (i == b) {
			cout << "[" << r[i] << "]";
		} else {
			cout << " " << r[i] << " " ;
		}
		cout << " ";
	}
	cout << endl;
#endif
}

int main() {
	string line;
	while (true) {
		cin >> n;
		if (cin.eof()) break;
		a = 0; b = 1;
		r.clear();
		r.push_back(3);
		r.push_back(7);
	
		while (r.size() < n + 10) {
			int m = r[a] + r[b];
			if (m >= 10) {
				r.push_back(m / 10);
			}
			r.push_back(m % 10);
			a = (a + r[a] + 1) % r.size();
			b = (b + r[b] + 1) % r.size();
			print();
		}
		forrr (i, n, n+10) {
			cout << r[i];
		}
		cout << endl;
	}

	return 0;
}
