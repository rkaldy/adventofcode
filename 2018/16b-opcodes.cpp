#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;


typedef enum { addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr } op_t;

void eval(op_t op, vector<int>& arg, vector<int>& rb, vector<int>& ra) {
	ra = rb;
	switch (op) {
		case addr: ra[arg[2]] = rb[arg[0]] + rb[arg[1]]; break;
		case addi: ra[arg[2]] = rb[arg[0]] + arg[1]; break;
		case mulr: ra[arg[2]] = rb[arg[0]] * rb[arg[1]]; break;
		case muli: ra[arg[2]] = rb[arg[0]] * arg[1]; break;
		case banr: ra[arg[2]] = rb[arg[0]] & rb[arg[1]]; break;
		case bani: ra[arg[2]] = rb[arg[0]] & arg[1]; break;
		case borr: ra[arg[2]] = rb[arg[0]] | rb[arg[1]]; break;
		case bori: ra[arg[2]] = rb[arg[0]] | arg[1]; break;
		case setr: ra[arg[2]] = rb[arg[0]]; break;
		case seti: ra[arg[2]] = arg[0]; break;
		case gtir: ra[arg[2]] = arg[0] > rb[arg[1]] ? 1 : 0; break;
		case gtri: ra[arg[2]] = rb[arg[0]] > arg[1] ? 1 : 0; break;
		case gtrr: ra[arg[2]] = rb[arg[0]] > rb[arg[1]] ? 1 : 0; break;
		case eqir: ra[arg[2]] = arg[0] == rb[arg[1]] ? 1 : 0; break;
		case eqri: ra[arg[2]] = rb[arg[0]] == arg[1] ? 1 : 0; break;
		case eqrr: ra[arg[2]] = rb[arg[0]] == rb[arg[1]] ? 1 : 0; break;
	}
}

int main() {
	int opi;
	vector<int> rb(4), ra(4), rax(4);
	vector<int> arg(3);
	string line;

	set<op_t> mapping[16];
	forr (i, 16) {
		forr (j, 16) {
			mapping[i].insert((op_t)j);
		}
	}

	while (true) {
		getline(cin, line);
		if (line.empty()) break;
		sscanf(line.c_str(), "Before: [%i, %i, %i, %i]", &rb[0], &rb[1], &rb[2], &rb[3]);
		getline(cin, line);
		sscanf(line.c_str(), "%i %i %i %i", &opi, &arg[0], &arg[1], &arg[2]);
		getline(cin, line);
		sscanf(line.c_str(), "After:  [%i, %i, %i, %i]", &rax[0], &rax[1], &rax[2], &rax[3]);
		getline(cin, line);

		int match = 0;
		forr(op, 16) {
			eval((op_t)op, arg, rb, ra);
			//LOG(PAR(opi) << PAR(arg) << PAR(rb) << PAR(ra) << PAR(rax));
			if (ra != rax) {
				mapping[opi].erase((op_t)op);
			}
		}
	}

	bool deleted = false;
	do {
		deleted = false;
		forr(i, 16) {
			if (mapping[i].size() == 1) {
				op_t op = *mapping[i].begin();
				forr(j, 16) {
					if (j != i && mapping[j].size() > 1 && has(mapping[j], op)) {
						mapping[j].erase(op);
						deleted = true;
					}
				}
			}
		}
		forr(i, 16) LOG(mapping[i]);
		LOG("");
	} while (deleted);

	rb = { 0, 0, 0, 0 };
	while (scanf("%i %i %i %i\n", &opi, &arg[0], &arg[1], &arg[2]) == 4) {
		eval(*mapping[opi].begin(), arg, rb, ra);
		rb = ra;
		LOG(PAR(opi) << PAR(arg) << PAR(ra) << PAR(rb));
	}
	cout << rb[0] << endl;
	return 0;
}
