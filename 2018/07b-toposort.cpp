#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

bool g[26][26];
int maxv;

void print() {
#ifdef LOGGING
	forr (i, maxv) {
		forr (j, maxv) {
			cerr << (g[i][j] ? '*' : '.');
		}
		cerr << endl;
	}
	cerr << endl;
#endif
}

int main() {
	string line;
	while (getline(cin, line)) {
		char a, b;
		sscanf(line.c_str(), "Step %c must be finished before step %c can begin.", &a, &b);
		a -= 'A';
		b -= 'A';
		g[a][b] = true;
		if (max(a,b) > maxv) {
			maxv = max(a,b);
		}
	}
	maxv++;

	print();
	set<int> vs;
	vector<int> dur(maxv, 0);
	forr (i, maxv) vs.insert(i);

	while (!vs.empty()) {
		for (int j : vs) {
			bool stok = true;
			forr (i, maxv) {
				if (g[i][j]) {
					stok = false;
					break;
				}
			}
			if (stok) {
				LOG(PAR(j));
				forr (i, maxv) {
					if (g[j][i]) {
						if (dur[i] < dur[j] + j + 61) {
							dur[i] = dur[j] + j + 61;
						}
						g[j][i] = false;
					}
				}
				LOG(PAR(dur));
				vs.erase(j);
				continue;
			}
		}
		print();
	}

	forr (i, maxv) {
		dur[i] += i + 61;
	}
	cout << max(dur) << endl;

	return 0;
}
