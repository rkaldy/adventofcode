#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;


typedef struct {
	int x, y;
	int vx, vy;
} star;

int main() {
	vector<star> stars;
	star s;
	while (scanf("position=<%i, %i> velocity=<%i, %i>\n", &s.x, &s.y, &s.vx, &s.vy) == 4) {
		stars.push_back(s);
	}

	int osp = INT_MAX;
	int time = 0;
	while (true) {
		int xmin = min(stars, &star::x);
		int xmax = max(stars, &star::x);
		int ymin = min(stars, &star::y);
		int ymax = max(stars, &star::y);
		int sp = xmax-xmin + ymax-ymin;
		LOG(PAR(xmax-xmin) << PAR(ymax-ymin) << PAR(sp));
		if (sp > osp) {
			for (star& s : stars) {
				s.x -= s.vx;
				s.y -= s.vy;
			}
			bool board[ymax-ymin+1][xmax-xmin+1];
			zero(board);
			for (star& s : stars) {
				board[s.y-ymin][s.x-xmin] = true;
			}
			forr (y, ymax-ymin+1) {
				forr (x, xmax-xmin+1) {
					if (board[y][x]) cout << '#';
					else cout << '.';
				}
				cout << endl;
			}
			cout << time-1 << endl;
			break;
		}
		osp = sp;
		time++;
		for (star& s : stars) {
			s.x += s.vx;
			s.y += s.vy;
		}
	}

	return 0;
}
