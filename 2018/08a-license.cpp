#include "lib/codejam.h"

using namespace std;


int readnode() {
	int children, metadata;
	int value = 0;
	cin >> children >> metadata;
	forr (i, children) {
		value += readnode();
	}

	int meta;
	forr (i, metadata) {
		cin >> meta;
		value += meta;
	}
	return value;
}


int main() {
	cout << readnode() << endl;

	return 0;
}
