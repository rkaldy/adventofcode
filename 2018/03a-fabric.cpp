#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <bitset>

using namespace std;


typedef struct {
	int x, y, h, w;
} rect;

const int SZ = 1000;

int main() {
	vector<rect> rects;
	char line[100];
	while (true) {
		rect r;
		int id;
		cin.getline(line, 100);
		if (cin.eof()) break;
		sscanf(line, "#%i @ %i,%i: %ix%i", &id, &r.x, &r.y, &r.w, &r.h);
		rects.push_back(r);
	}

	short canvas[SZ][SZ];
	memset(canvas, 0, sizeof(canvas));

	for (rect& r : rects) {
		for (int y = r.y; y < r.y+r.h; y++) {
			for (int x = r.x; x < r.x+r.w; x++) {
				canvas[y][x]++;
			}
		}
	}

	int ret = 0;
	for (int y = 0; y < SZ; y++) {
		for (int x = 0; x < SZ; x++) {
			if (canvas[y][x] > 1) ret++;	
		}
	}
	cout << ret << endl;

	return 0;
}
