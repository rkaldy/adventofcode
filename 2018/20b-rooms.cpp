#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

int b[5000][5000];

void run(const string& path, int& i, int sx, int sy) {
	int x = sx;
	int y = sy;
	do {
		if (path[i] == '(') {
			i++;
			run(path, i, x, y);
		}
		else if (path[i] == '|') {
			x = sx; y = sy;
		}
		else if (isalpha(path[i])) {
			int len = b[y][x] + 1;
			switch (path[i]) {
				case 'N': y--; break;
				case 'S': y++; break;
				case 'E': x++; break;
				case 'W': x--; break;
			}
			if (len < b[y][x]) b[y][x] = len;
		}
		i++;
	} while (path[i] != ')' && path[i] != '$');
}

int main() {
	string line;	
	while (true) {
		getline(cin, line);
		if (cin.eof()) break;

		forr(y, 5000) forr(x, 5000) b[y][x] = INT_MAX;
		b[2500][2500] = 0;
		int i = 1;
		run(line, i, 2500, 2500);
		int count = 0;
		forr(y, 5000) forr(x, 5000) if (b[y][x] != INT_MAX && b[y][x] >= 1000) count++;
		cout << count << endl;
	}
	return 0;
}
