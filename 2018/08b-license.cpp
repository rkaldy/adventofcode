#include "lib/codejam.h"

using namespace std;


int readnode() {
	int children, metadata;
	cin >> children >> metadata;
	vector<int> childValues;
	forr (i, children) {
		childValues.push_back(readnode());
	}

	int meta;
	int value = 0;
	forr (i, metadata) {
		cin >> meta;
		if (children == 0) {
			value += meta;
		} else {
			if (meta > 0 && meta <= children) {
				value += childValues[meta - 1];
			}
		}
	}
	LOG(value);
	return value;
}


int main() {
	cout << readnode() << endl;
	return 0;
}
