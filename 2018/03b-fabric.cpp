#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <bitset>

using namespace std;


typedef struct {
	int x, y, h, w;
} rect;

const int SZ = 1000;

int main() {
	vector<rect> rects;
	char line[100];
	while (true) {
		rect r;
		int id;
		cin.getline(line, 100);
		if (cin.eof()) break;
		sscanf(line, "#%i @ %i,%i: %ix%i", &id, &r.x, &r.y, &r.w, &r.h);
		rects.push_back(r);
	}
	
	bool overlap[rects.size()] = { false };
	for (int i = 1; i < rects.size(); i++) {
		rect& r = rects[i];
		for (int j = 0; j < i; j++) {
			rect& s = rects[j];
			if (((r.x >= s.x && r.x < s.x+s.w) || (s.x >= r.x && s.x < r.x+r.w))
			 && ((r.y >= s.y && r.y < s.y+s.h) || (s.y >= r.y && s.y < r.y+r.h))) {
				overlap[i] = overlap[j] = true;
			}
		}
	}

	for (int i = 0; i < rects.size(); i++) {
		if (!overlap[i]) cout << i+1 << endl;
	}

	return 0;
}
