#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

int n, a, b;
vector<int> r, seq;

void print() {
#ifdef LOGGING
	forr (i, r.size()) {
		if (i == a) {
			cout << "(" << r[i] << ")";
		} else if (i == b) {
			cout << "[" << r[i] << "]";
		} else {
			cout << " " << r[i] << " " ;
		}
		cout << " ";
	}
	cout << endl;
#endif
}

bool match() {
	if (r.size() < seq.size()) return false;
	forr (i, seq.size()) {
		if (r[r.size() - seq.size() + i] != seq[i]) return false;
	}
	return true;
}

int main() {
	string line;
	while (getline(cin, line)) {
		seq.clear();
		forr (i, line.size()) {
			seq.push_back(line[i] - '0');
		}
		LOG(PAR(seq));
		
		a = 0; b = 1;
		r.clear();
		r.push_back(3);
		r.push_back(7);
	
		while (true) {
			int m = r[a] + r[b];
			if (m >= 10) {
				r.push_back(m / 10);
				if (match()) break;
			}
			r.push_back(m % 10);
			if (match()) break;

			a = (a + r[a] + 1) % r.size();
			b = (b + r[b] + 1) % r.size();
			print();
		} 
		cout << r.size() - seq.size() << endl;
	}

	return 0;
}
