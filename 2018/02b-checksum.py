#!/usr/bin/python3

import sys, numpy
from collections import defaultdict
from recordclass import recordclass

ids = []
with open("02.in", "r") as f:
    for line in f:
        ids.append(line[:-1])

for i in range(len(ids)):
    for j in range(i):
        if len(ids[i]) != len(ids[j]):
            continue
        diff = 0
        for k in range(len(ids[i])):
            if ids[i][k] != ids[j][k]:
                diff += 1
            if diff > 1:
                break
        if diff == 1:
            for k in range(len(ids[i])):
                if ids[i][k] == ids[j][k]:
                    sys.stdout.write(ids[i][k])
            sys.stdout.write("\n")
            sys.exit(0)
                
