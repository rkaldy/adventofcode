#!/usr/bin/python3

import sys, numpy
from collections import defaultdict
from recordclass import recordclass

freq = 0
s = {}
while True:
    with open("01.in", "r") as f:
        for line in f:
            if line[0] == "+":
                freq += int(line[1:])
            else:
                freq -= int(line[1:])
            if freq in s:
                print("Result: %i" % freq)
                sys.exit(0)
            s[freq] = 1
