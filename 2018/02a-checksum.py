#!/usr/bin/python3

import sys, numpy
from collections import defaultdict
from recordclass import recordclass

with open("02.in", "r") as f:
    c2 = 0
    c3 = 0
    for line in f:
        ltrs = {}
        h2 = False
        h3 = False
        for c in line[:-1]:
            if c in ltrs:
                ltrs[c] += 1
            else:
                ltrs[c] = 1
        for c, f in ltrs.items():
            if f == 2:
                h2 = True
            if f == 3:
                h3 = True
        if h2:
            c2 += 1
        if h3:
            c3 += 1

    print(c2*c3)
