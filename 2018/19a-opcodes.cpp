#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;


typedef enum { addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr } op_t;
const string OPCODES[] = { "addr", "addi", "mulr", "muli", "banr", "bani", "borr", "bori", "setr", "seti", "gtir", "gtri", "gtrr", "eqir", "eqri", "eqrr" };
map<string, op_t> OPS;

typedef struct {
	op_t op;
	vector<int> arg;
} instr_t;
  

void eval(op_t op, vector<int> arg, vector<int>& rb, vector<int>& ra) {
	ra = rb;
	switch (op) {
		case addr: ra[arg[2]] = rb[arg[0]] + rb[arg[1]]; break;
		case addi: ra[arg[2]] = rb[arg[0]] + arg[1]; break;
		case mulr: ra[arg[2]] = rb[arg[0]] * rb[arg[1]]; break;
		case muli: ra[arg[2]] = rb[arg[0]] * arg[1]; break;
		case banr: ra[arg[2]] = rb[arg[0]] & rb[arg[1]]; break;
		case bani: ra[arg[2]] = rb[arg[0]] & arg[1]; break;
		case borr: ra[arg[2]] = rb[arg[0]] | rb[arg[1]]; break;
		case bori: ra[arg[2]] = rb[arg[0]] | arg[1]; break;
		case setr: ra[arg[2]] = rb[arg[0]]; break;
		case seti: ra[arg[2]] = arg[0]; break;
		case gtir: ra[arg[2]] = arg[0] > rb[arg[1]] ? 1 : 0; break;
		case gtri: ra[arg[2]] = rb[arg[0]] > arg[1] ? 1 : 0; break;
		case gtrr: ra[arg[2]] = rb[arg[0]] > rb[arg[1]] ? 1 : 0; break;
		case eqir: ra[arg[2]] = arg[0] == rb[arg[1]] ? 1 : 0; break;
		case eqri: ra[arg[2]] = rb[arg[0]] == arg[1] ? 1 : 0; break;
		case eqrr: ra[arg[2]] = rb[arg[0]] == rb[arg[1]] ? 1 : 0; break;
	}
}

int main() {
	int ipr;
	instr_t in;
	in.arg.resize(3);
	string opcode;
	vector<instr_t> ins;

	forr(i, 16) {
		OPS[OPCODES[i]] = (op_t)i;
	}

	scanf("#ip %i\n", &ipr);
	while (true) {
		cin >> opcode >> in.arg[0] >> in.arg[1] >> in.arg[2];
		if (cin.eof()) break;
		in.op = OPS[opcode];
		ins.push_back(in);
	}
	
	vector<int> rb(6), ra(6);
	int& ip = rb[ipr];
	while (ip >= 0 && ip < ins.size()) {
		eval(ins[ip].op, ins[ip].arg, rb, ra);
		LOG(PAR(ip) << PAR(rb) << OPCODES[ins[ip].op] << " " << ins[ip].arg << " " << PAR(ra));
		rb = ra;
		ip++;
	}

	cout << rb[0] << endl;
	return 0;
}
