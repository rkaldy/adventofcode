#!/usr/bin/python3

import sys, numpy
from collections import defaultdict
from recordclass import recordclass

with open("01.in", "r") as f:
    freq = 0
    for line in f:
        if line[0] == "+":
            freq += int(line[1:])
        else:
            freq -= int(line[1:])
print(freq)
