#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

int main() {
	int c, e, le;
	set<int> es;

	e = 0;
	while (true) {
		c = e | 65536;
		e = 6152285;
		while (true) {
			e += c & 255;
			e &= 16777215;
			e *= 65899;
			e &= 16777215;
			if (c < 256) break;
			c /= 256;
		}
		LOG(PAR(e));
		if (has(es, e)) {
			cout << le << endl;
			break;
		}
		es.insert(e);
		le = e;
	}
	return 0;
}
