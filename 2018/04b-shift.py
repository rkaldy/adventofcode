#!/usr/bin/python3

import sys, numpy
from collections import defaultdict
from recordclass import recordclass


sleeps = {}
minutes = {}
gid = 0
asleep = 0
with open("04.in", "r") as f:
    for line in f:
        action = line[19:24]
        time = int(line[15:17])
        if action == "Guard":
            gid = int(line[26:].split(" ")[0])
            if gid not in sleeps:
                sleeps[gid] = 0
                minutes[gid] = [0] * 60
        elif action == "falls":
            asleep = time
        elif action == "wakes":
            sleeps[gid] += time - asleep
            for i in range(asleep, time):
                minutes[gid][i] += 1

gmaxmin = 0
maxgid = gid
for gid, minute in minutes.items():
    maxmin = 0
    for i in range(60):
        if minute[i] > minute[maxmin]:
            maxmin = i
    if minutes[gid][maxmin] > minutes[maxgid][gmaxmin]:
        maxgid = gid
        gmaxmin = maxmin

print("maxgid=%i maxmin=%i res=%i" % (maxgid, gmaxmin, maxgid*gmaxmin))
