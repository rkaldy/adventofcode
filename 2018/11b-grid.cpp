#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

int power(int x, int y, int sn) {
	return ((x+10) * y + sn) * (x+10) % 1000 / 100 - 5;
}

int main() {
	int sn;

	while (!cin.eof()) {
		cin >> sn;

		int xmax, ymax, nmax, pmax = INT_MIN;
		forrr (y, 1, 301) {
			forrr (x, 1, 301) {
				int p = 0;
				forr (n, 301-max(x,y)) {
					forr (v, n) {
						p += power(x+v, y+n, sn);
						p += power(x+n, y+v, sn);
					}
					p += power(x+n, y+n, sn);
					if (p > pmax) {
						xmax = x;
						ymax = y;
						nmax = n+1;
						pmax = p;
					}
				}
			}
		}
		cout << xmax << "," << ymax << "," << nmax << endl;
	}

	return 0;
}
