#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

int main() {
	int players, marbles;
	while (scanf("%i players; last marble is worth %i points\n", &players, &marbles) == 2) {
		vector<int> score(players, 0);
		list<int> b;
		b.push_back(0);
		list<int>::iterator i = b.begin();
		forrr (m, 1, marbles+1) {
			if (m % 23 == 0) {
				int p = (m - 1) % players;
				score[p] += m;
				forr (j, 7) {
					if (i == b.begin()) i = b.end();
					i--;
				}
				score[p] += *i;
				i = b.erase(i);
			} else {
				forr (k, 2) {
					if (i == b.end()) i = b.begin();
					i++;
				}
				i = b.insert(i, m);
			}
#ifdef LOGGING
			for (auto j = b.begin(); j != b.end(); j++) {
				if (j == i) {
					cout << "(" << *j << ") ";
				} else {
					cout << *j << " ";
				}
			}
			cout << endl;
#endif
		}
		cout << max(score) << endl;
	}

	return 0;
}
