#include "lib/codejam.h"
#include "lib/point3d.h"
#include "lib/dijkstra.h"

using namespace std;

#define XSIZE 12
#define YSIZE 12

int tx, ty, depth;
int b[YSIZE][XSIZE];
ipoint pr[YSIZE][XSIZE][3];


int el(int gi) { return (gi + depth) % 20183 % 3; }

void print() {
#ifdef LOGGING
	char m[YSIZE][XSIZE];
	forr (y, YSIZE) forr (x, XSIZE) m[y][x] = el(b[y][x]);
	ipoint p(tx, ty, 1);
	while (p != ipoint(0, 0, 1)) {
		cout << p << endl;
	//	m[p.y][p.x] = 3;
		p = pr[p.y][p.x][p.z];
	}
	forr (y, YSIZE) {
		forr (x, XSIZE) {
			switch (m[y][x]) {
				case 0: cout << '.'; break;
				case 1: cout << '='; break;
				case 2: cout << '|'; break;
				case 3: cout << 'X'; break;
			}
		}
		cout << endl;
	}
	cout << endl;
#endif
}


int main() {

	scanf("depth: %i\n", &depth);
	scanf("target: %i,%i\n", &tx, &ty);

	b[0][0] = 0;
	forrr (x, 1, XSIZE) b[0][x] = (x * 16807) % 20183;
	forrr (y, 1, YSIZE) b[y][0] = (y * 48271) % 20183;
	forrr (y, 1, YSIZE) {
		forrr (x, 1, XSIZE) {
			b[y][x] = ((b[y-1][x] + depth) * (b[y][x-1] + depth)) % 20183;
		}
	}

	fibonacci_heap<ipoint, int, XSIZE*YSIZE*3> fh(
		[](const ipoint p) { return (p.y*XSIZE+p.x)*3+p.z; },
		[](int n, ipoint& p) { p.y = n/3/XSIZE; p.x = (n/3)%XSIZE; p.z = n%3; }
	);
	fh.init(ipoint(0, 0, 1));
	ipoint p, target(tx, ty, 1);
	int dist;
	while (true) {
		dist = fh.pop(p);
		if (p == target) break;
		forr (dir, 4) {
			ipoint q = p + DIR[dir];
			if (q.x < 0 || q.x >= XSIZE || q.y < 0 || q.y >= YSIZE) continue;
			for (q.z = 0; q.z < 3; q.z++) {
				if (el(b[q.y][q.x]) == q.z) continue;
				if (fh.update(q, dist + (p.z == q.z ? 1 : 8))) {
					pr[q.y][q.x][q.z] = p;
				}
			}
		}
	}
	print();

	cout << dist << endl;
	return 0;
}
