#include "lib/codejam.h"
#include "lib/point.h"

using namespace std;


#define EMPTY 0
#define WALL 1
#define MAN 2
#define STEP 3

#define ELF 1
#define GOBLIN 2

#define ATTACK 3
#define HP_INIT 200

const ipoint RDIR[4] = { DIR[3], DIR[2], DIR[0], DIR[1] };

typedef struct {
	ipoint pos;
	int hp, type;
} man;

typedef struct {
	char type;
	man* m;
} field;

field board[100][100];
int sizex, sizey;
int turn;
int elf_count, goblin_count;


void print() {
#ifdef LOGGING
	forr (y, sizey) {
		forr (x, sizex) {
			field f = board[y][x];
			char c;
			switch (f.type) {
				case EMPTY: c = '.'; break;
				case WALL:  c = '#'; break;
				case STEP:  c = '+'; break;
				case MAN:   c = f.m->type == ELF ? 'E' : 'G';
			}
			cout << c;
		}
		forr (x, sizex) {
			field f = board[y][x];
			if (f.type == MAN) {
				cout << "  " << (f.m->type == ELF ? "E" : "G") << "(" << f.m->hp << ")";
			}
		}
		cout << endl;
	}
	cout << endl;
#endif
}


void clean() {
	forr (y, sizey) {
		forr (x, sizex) {
			if (board[y][x].type == STEP) board[y][x].type = EMPTY;
		}
	}
}


void attack(man* m) {
	man* e = NULL;
	int edir = -1;

	forr (i, 4) {
		ipoint p = m->pos + RDIR[i];
		field f = board[p.y][p.x];
		if (f.type == MAN) {
			if (f.m->type == 3 - m->type) {
				if (!e || f.m->hp < e->hp) {
					e = f.m;
					edir = i;
				}
			}
		}
	}
	if (e) {
		e->hp -= ATTACK;
		if (e->hp <= 0) {
			board[e->pos.y][e->pos.x] = { EMPTY, NULL };
			if (e->type == ELF) elf_count--;
			else goblin_count--;
		}
	}
}


void mmove(man* m) {
	queue<pair<int, ipoint>> step;

	forr (i, 4) {
		ipoint p = m->pos + RDIR[i];
		field& f = board[p.y][p.x];
		if (f.type == MAN && f.m->type == 3 - m->type) {
			clean();
			return;
		}
		else if (f.type == EMPTY) {
			f.type = STEP;
			step.push(mp(i, p));
		}
	}
	while (!step.empty()) {
		int dir = step.front().first;
		ipoint p = step.front().second;
		step.pop();
		forr (i, 4) {
			ipoint q = p + RDIR[i];
			field& f = board[q.y][q.x];
			if (f.type == MAN && f.m->type == 3 - m->type) {
				board[m->pos.y][m->pos.x] = { EMPTY, NULL };
				m->pos += RDIR[dir];
				board[m->pos.y][m->pos.x] = { MAN, m };
				clean();
				return;
			}
			else if (f.type == EMPTY) {
				f.type = STEP;
				step.push(mp(dir, q));
			}
		}
	}
	clean();
}


int main() {
	string line;
	vector<man> men;
	vector<man*> men_sort;
	men.reserve(50);
	while (getline(cin, line)) {
		sizex = line.size();
		forr (x, sizex) {
			switch (line[x]) {
				case '.': board[sizey][x] = { EMPTY, NULL }; break;
				case '#': board[sizey][x] = { WALL, NULL }; break;
				case 'E':
				case 'G':
					men.push_back({ ipoint(x, sizey), HP_INIT, line[x] == 'E' ? ELF : GOBLIN });
					men_sort.push_back(&men.back());
					board[sizey][x] = { MAN, &men.back() };
					if (line[x] == 'E') elf_count++;
					else goblin_count++;
			}
		}
		sizey++;
	}
	print();

	do {
		sort(men_sort.begin(), men_sort.end(), [](const man* a, const man* b) { return a->pos < b->pos; });
		int i = 0;
		for (; i < men_sort.size(); i++) {
			if (men_sort[i]->hp > 0) {
				mmove(men_sort[i]);
				attack(men_sort[i]);
			}
			if (elf_count == 0 || goblin_count == 0) break;
		}
		
		if (i >= men_sort.size() - 1) {
			turn++;
		}
		LOG("After turn #" << turn);
		print();
	} while (elf_count > 0 && goblin_count > 0);

	int sum = 0;
	for (auto& m : men) {
		if (m.hp > 0) sum += m.hp;
	}
	cout << sum*turn << endl;
	return 0;
}
