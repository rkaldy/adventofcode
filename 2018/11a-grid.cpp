#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;

int power(int x, int y, int sn) {
	return ((x+10) * y + sn) * (x+10) % 1000 / 100;
}

int main() {
	int sn;

	while (!cin.eof()) {
		cin >> sn;

		int xmax, ymax, pmax = INT_MIN;
		forrr (y, 1, 299) {
			forrr (x, 1, 299) {
				int p = 0;
				forr (sy, 3) {
					forr (sx, 3) {
						p += power(x+sx, y+sy, sn);
					}
				}
				if (p > pmax) {
					xmax = x;
					ymax = y;
					pmax = p;
				}
			}
		}
		cout << xmax << "," << ymax << endl;
	}

	return 0;
}
