#!/usr/bin/python3

import sys, networkx

g = networkx.DiGraph()
for line in sys.stdin:
    line = line.split(" ")
    a = line[1]
    b = line[7]
    g.add_edge(a, b, weight=ord(a)-ord('A')+61)

res = list(networkx.algorithms.dag.lexicographical_topological_sort(g))
cp = networkx.algorithms.dag.dag_longest_path_length(g) + ord(res[-1])-ord('A')+61

print("".join(res))
print(cp)
