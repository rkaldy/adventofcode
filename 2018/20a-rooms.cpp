#include "lib/codejam.h"
#include "lib/minimize.h"

using namespace std;


pair<int, bool> run(const string& path, int& i) {
	int maxlen = 0;
	int len = 0;
	do {
		if (path[i] == '(') {
			i++;
			pair<int, bool> ret = run(path, i);
			if (ret.second) {
				if (len + ret.first > maxlen) maxlen = len + ret.first;
			} else {
				len += ret.first;
			}
		}
		else if (path[i] == '|') {
			if (path[i+1] == ')') {
				i++;
				return mp(len / 2, true);
			} 
			if (len > maxlen) maxlen = len;
			len = 0;
		}
		else if (isalpha(path[i])) {
			len++;
		}
		i++;
	} while (path[i] != ')' && path[i] != '$');
	if (len > maxlen) maxlen = len;
	return mp(maxlen, false);
}

int main() {
	string line;	
	while (true) {
		getline(cin, line);
		if (cin.eof()) break;
		int i = 1;
		pair<int, bool> ret = run(line, i);
		cout << ret.first << endl;
	}
	return 0;
}
