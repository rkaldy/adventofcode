#include "lib/codejam.h"
#include "lib/point.h"

using namespace std;


typedef enum { LEFT, STRAIGHT, RIGHT } turn;

class cart : public ipoint {
public:
	cart(int ax, int ay, int ad, int at) : ipoint(ax, ay), d(ad), t(at) {}
	int d;
	int t;
};


char b[150][150];
vector<cart> carts;
int xs, ys;

const char TRACKS[] = { '-', '|', '/', '\\', '+', ' ' };
const char CARTS[] = { '>', 'v', '<', '^' };


void print() {
#ifdef LOGGING
	static char b2[150][150];
	memcpy(b2, b, sizeof(b));
	for (cart& c : carts) {
		b2[c.y][c.x] = CARTS[c.d];
	}
	forr (y, ys) {
		cerr << b2[y] << endl;
	}
	cerr << endl;
#endif
}

int main() {
	string line;
	ys = 0;
	while (getline(cin, line)) {
		char c;
		for (xs = 0; xs < line.size(); xs++) {
			char p = line[xs];
			forr (i, 6) {
				if (p == TRACKS[i]) {
					b[ys][xs] = p;
					break;
				}
			}
			forr (i, 4) {
				if (p == CARTS[i]) {
					b[ys][xs] = TRACKS[i % 2];
					carts.emplace_back(xs, ys, i, LEFT);
					break;
				}
			}
		}
		ys++;
	}
	xs--;

	while (carts.size() > 1) {
		print();
		sort(carts.begin(), carts.end(), [](const cart& a, const cart& b) { return (a.y < b.y) || (a.y == b.y && a.x < b.x); });
		for (cart& c : carts) {
			if (c.d == -1) continue;
			c += DIRS[c.d];
			char p = b[c.y][c.x];
			if (p == '/' || p == '\\') {
				if ((p == '\\') == (c.d % 2 == 0)) {
					c.d = (c.d + 1) % 4;
				} else {
					c.d = (c.d + 3) % 4;
				}
			}
			else if (p == '+') {
				if (c.t == LEFT) {
					c.d = (c.d + 3) % 4;
				} else if (c.t == RIGHT) {
					c.d = (c.d + 1) % 4;
				}
				c.t = (c.t + 1) % 3;
			}
			for (cart& d : carts) {
				if (&c != &d && c.x == d.x && c.y == d.y) {
					LOG("collision " << PAR(c.x) << PAR(c.y));
					c.d = d.d = -1;
					break;
				}
			}
		}
		for (auto i = carts.begin(); i != carts.end();) {
			LOG(PAR(i->x) << PAR(i->y) << PAR(i->d));
			if (i->d == -1) {
				LOG("removing cart at " << i->x << "," << i->y);
				i = carts.erase(i);
			} else i++;
		}
	}
	cout << carts[0].x << "," << carts[0].y << endl;

	return 0;
}
