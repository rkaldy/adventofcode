#include "lib/codejam.h"
#include "lib/point.h"

using namespace std;


#define EMPTY 0
#define WALL 1
#define MAN 2
#define STEP 3

#define ELF 1
#define GOBLIN 2

#define GOBLIN_ATTACK 3
#define HP_INIT 200

const ipoint RDIR[4] = { DIR[3], DIR[2], DIR[0], DIR[1] };

typedef struct {
	ipoint pos;
	int hp, type;
} man;

typedef struct {
	char type;
	man* m;
} field;

typedef struct {
	ipoint pos;
	int dir, dist;
} step;


field board[100][100];
int sizex, sizey;
int turn;
int elf_attack;
int goblin_count;
vector<man*> men_sort;


void print() {
#ifdef LOGGING
	forr (y, sizey) {
		forr (x, sizex) {
			field f = board[y][x];
			char c;
			switch (f.type) {
				case EMPTY: c = '.'; break;
				case WALL:  c = '#'; break;
				case STEP:  c = '+'; break;
				case MAN:   c = f.m->type == ELF ? 'E' : 'G';
			}
			cout << c;
		}
		forr (x, sizex) {
			field f = board[y][x];
			if (f.type == MAN) {
				cout << "  " << (f.m->type == ELF ? "E" : "G") << "(" << f.m->hp << ")";
			}
		}
		cout << endl;
	}
	cout << endl;
#endif
}


void clean() {
	forr (y, sizey) {
		forr (x, sizex) {
			if (board[y][x].type == STEP) board[y][x].type = EMPTY;
		}
	}
}


bool attack(man* m) {
	man* e = NULL;
	int edir = -1;

	forr (i, 4) {
		ipoint p = m->pos + RDIR[i];
		field f = board[p.y][p.x];
		if (f.type == MAN) {
			if (f.m->type == 3 - m->type) {
				if (!e || f.m->hp < e->hp) {
					e = f.m;
					edir = i;
				}
			}
		}
	}
	if (e) {
		if (m->type == ELF) e->hp -= elf_attack;
		else e->hp -= GOBLIN_ATTACK;
		if (e->hp <= 0) {
			board[e->pos.y][e->pos.x] = { EMPTY, NULL };
			if (e->type == ELF) return false;
			else goblin_count--;
		}
	}
	return true;
}


void mmove(man* m) {
	queue<step> steps;
	set<ipoint> range;
	vector<step> reach;

	for (auto n : men_sort) {
		if (n->hp > 0 && n->type == 3 - m->type) {
			forr (i, 4) {
				ipoint p = n->pos + DIR[i];
				if (board[p.y][p.x].type == EMPTY) {
					range.insert(p);
				}
			}
		}
	}

	forr (i, 4) {
		ipoint p = m->pos + RDIR[i];
		field& f = board[p.y][p.x];
		if (f.type == MAN && f.m->type == 3 - m->type) {
			clean();
			return;
		}
		else if (f.type == EMPTY) {
			f.type = STEP;
			steps.push({p, i, 1});
		}
	}

	int dist = INT_MAX;
	while (!steps.empty() && steps.front().dist <= dist) {
		step s = steps.front();
		steps.pop();
		if (has(range, s.pos)) {
			dist = s.dist;
			reach.push_back(s);
		}
		if (dist == INT_MAX) {
			forr (i, 4) {
				ipoint q = s.pos + RDIR[i];
				field& f = board[q.y][q.x];
				if (f.type == EMPTY) {
					f.type = STEP;
					steps.push({q, s.dir, s.dist + 1});
				}
			}
		}
	}

	sort(reach.begin(), reach.end(), [](const step& a, const step& b) { return a.pos < b.pos; });
	if (!reach.empty()) {
		board[m->pos.y][m->pos.x] = { EMPTY, NULL };
		m->pos += RDIR[reach.front().dir];
		board[m->pos.y][m->pos.x] = { MAN, m };
	}
	clean();
}


bool battle() {
	LOG("Battle begins " << PAR(elf_attack));
	print();
	do {
		sort(men_sort.begin(), men_sort.end(), [](const man* a, const man* b) { return a->pos < b->pos; });
		int i = 0;
		for (; i < men_sort.size(); i++) {
			if (men_sort[i]->hp > 0) {
				if (goblin_count == 0) break;
				mmove(men_sort[i]);
				if (!attack(men_sort[i])) {
					LOG("Elf dies");
					return false;
				}
			}
		}
		
		if (i >= men_sort.size() - 1) {
			turn++;
		}
		LOG("After turn #" << turn);
		print();
	} while (goblin_count > 0);
	LOG("Elves won");
	return true;
}


int main() {
	string line;
	field iboard[100][100];
	vector<man> imen;
	while (getline(cin, line)) {
		sizex = line.size();
		forr (x, sizex) {
			switch (line[x]) {
				case '.': iboard[sizey][x] = { EMPTY, NULL }; break;
				case '#': iboard[sizey][x] = { WALL, NULL }; break;
				case 'E':
				case 'G':
					imen.push_back({ ipoint(x, sizey), HP_INIT, line[x] == 'E' ? ELF : GOBLIN });
					iboard[sizey][x] = { MAN, NULL };
			}
		}
		sizey++;
	}

	vector<man> men;
	elf_attack = GOBLIN_ATTACK;
	do {
		memcpy(board, iboard, 100*100*sizeof(field));
		men = imen;
		men_sort.clear();
		goblin_count = 0;
		turn = 0;
		for (man& m : men) {
			if (m.type == GOBLIN) goblin_count++;
			men_sort.push_back(&m);
			board[m.pos.y][m.pos.x] = { MAN, &m };
		}
		elf_attack++;
	} while (!battle());

	int sum = 0;
	for (auto& m : men) {
		if (m.hp > 0) sum += m.hp;
	}
	cout << sum*turn << endl;
	return 0;
}
