#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include "lib/codejam.h"

using namespace std;

int main() {
	string pol = readfile("05.in");
	if (pol[pol.size() - 1] == '\n') {
		pol = pol.substr(0, pol.size() - 1);
	}
	int len = pol.size();
	int first = 0, next = 1;
	do {
//		cout << PAR(first) << PAR(next) << PAR(pol) << endl;
		if ((pol[first] & 31) == (pol[next] & 31) && (pol[first] & 32) != (pol[next] & 32)) {
			pol[first] = pol[next] = ' ';
			len -= 2;
			while (first >= 0 && pol[first] == ' ') first--;
			while (next < pol.size() && pol[next] == ' ') next++;
			if (first >= 0 && next < pol.size()) continue;
		}
		first = next;
		next++;
		while (next < pol.size() && pol[next] == ' ') next++;
	} while (next < pol.size());
	cout << len << endl;

	return 0;
}
