#include "lib/codejam.h"

using namespace std;

bool g[26][26];
int maxv;

void print() {
#ifdef LOGGING
	forr (i, maxv) {
		forr (j, maxv) {
			cerr << (g[i][j] ? '*' : '.');
		}
		cerr << endl;
	}
	cerr << endl;
#endif
}

int main() {
	string line;
	while (getline(cin, line)) {
		char a, b;
		sscanf(line.c_str(), "Step %c must be finished before step %c can begin.", &a, &b);
		a -= 'A';
		b -= 'A';
		g[a][b] = true;
		if (max(a,b) > maxv) {
			maxv = max(a,b);
		}
	}
	maxv++;

	print();
	vector<int> res;
	set<int> vs;
	forr (i, maxv) vs.insert(i);

	while (!vs.empty()) {
		for (int j : vs) {
			bool stok = true;
			forr (i, maxv) {
				if (g[i][j]) {
					stok = false;
					break;
				}
			}
			if (stok) {
				LOG(PAR(j));
				forr (i, maxv) {
					g[j][i] = false;
				}
				vs.erase(j);
				res.push_back(j);
				print();
				break;
			}
		}
	}
	for (int i : res) {
		cout << (char)(i + 'A');
	}
	cout << endl;

	return 0;
}
