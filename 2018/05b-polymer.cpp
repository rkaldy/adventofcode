#include "lib/codejam.h"

using namespace std;

int main() {
	string ipol = readcin();
	if (ipol[ipol.size() - 1] == '\n') {
		ipol = ipol.substr(0, ipol.size() - 1);
	}

	int minlen = ipol.size();
	for (char u = 'a'; u <= 'z'; u++) {
		string pol = ipol;
		int len = pol.size();
		for (int i = 0; i < pol.size(); i++) {
			if ((pol[i] & 31) == (u & 31)) {
				pol[i] = ' ';
				len--;
			}
		}
		if (len == pol.size()) continue;

		LOG(PAR(u))
		int first = 0, next = 1;
		do {
			LOG(PAR(first) << PAR(next) << PAR(pol))
			if ((pol[first] & 31) == (pol[next] & 31) && (pol[first] & 32) != (pol[next] & 32)) {
				pol[first] = pol[next] = ' ';
				len -= 2;
				while (first >= 0 && pol[first] == ' ') first--;
				while (next < pol.size() && pol[next] == ' ') next++;
				if (first >= 0 && next < pol.size()) continue;
			}
			first = next;
			next++;
			while (next < pol.size() && pol[next] == ' ') next++;
		} while (next < pol.size());

		LOG(PAR(len));
		if (len < minlen) minlen = len;
	}
	cout << minlen << endl;

	return 0;
}
