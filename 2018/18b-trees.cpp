#include "lib/codejam.h"
#include "lib/minimize.h"
#include <unistd.h>

using namespace std;

#ifdef NDEBUG
#define SIZE 50
#else
#define SIZE 10
#endif
	
char b[SIZE][SIZE], b2[SIZE][SIZE], bm[SIZE][SIZE];

void print() {
#ifdef LOGGING
	forr(y, SIZE) {
		forr(x, SIZE) {
			cout << b[y][x];
		}
		cout << endl;
	}
	cout << endl;
	usleep(40000);
#endif
}

int main() {
	forr(y, SIZE) {
		string line;
		getline(cin, line);
		forr(x, SIZE) {
			b[y][x] = line[x];
		}
	}
	memcpy(b2, b, SIZE*SIZE);

	LOG("Initial state");
	print();

	forr(i, 1000000000) {
		forr(y, SIZE) {
			forr(x, SIZE) {
				int w = 0, l = 0;
				forrr(sy, y-1, y+2) {
					forrr (sx, x-1, x+2) {
						if (sy < 0 || sy >= SIZE || sx < 0 || sx >= SIZE || (sx == x && sy == y)) continue;
						switch (b[sy][sx]) {
							case '|': w++; break;
							case '#': l++; break;
						}
					}
				}
				switch (b[y][x]) {
					case '.': if (w >= 3) b2[y][x] = '|'; break;
					case '|': if (l >= 3) b2[y][x] = '#'; break;
					case '#': if (w == 0 || l == 0) b2[y][x] = '.'; break;
				}
			}
		}
		memcpy(b, b2, SIZE*SIZE);

		if (i == 5000) {
			memcpy(bm, b, SIZE*SIZE);
			cout << "Saved" << endl;
		} else if (i > 5000 && i < 10000) {
			if (memcmp(b, bm, SIZE*SIZE) == 0) {
				int r = i - 5000;
				cout << "r=" << r << endl;
				i += (1000000000 - i) / r * r;
			}
		}

		LOG("\033[2J\033[1;1H");
		LOG("After " << i << " minutes");
		print();
	}

	int w = 0, l = 0;
	forr(y, SIZE) {
		forr(x, SIZE) {
			if (b[y][x] == '|') w++;
			else if (b[y][x] == '#') l++;
		}
	}
	cout << w*l << endl;

	return 0;
}
