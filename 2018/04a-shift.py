#!/usr/bin/python3

import sys, numpy
from collections import defaultdict
from recordclass import recordclass


sleeps = {}
minutes = {}
gid = 0
asleep = 0
with open("04.in", "r") as f:
    for line in f:
        action = line[19:24]
        time = int(line[15:17])
        if action == "Guard":
            gid = int(line[26:].split(" ")[0])
            if gid not in sleeps:
                sleeps[gid] = 0
                minutes[gid] = [0] * 60
        elif action == "falls":
            asleep = time
        elif action == "wakes":
            sleeps[gid] += time - asleep
            for i in range(asleep, time):
                minutes[gid][i] += 1

maxgid = gid
for gid, sleep in sleeps.items():
    if sleep > sleeps[maxgid]:
        maxgid = gid

maxmin = 0
for i in range(60):
    if minutes[maxgid][i] > minutes[maxgid][maxmin]:
        maxmin = i

print("maxgid=%i maxmin=%i res=%i" % (maxgid, maxmin, maxgid*maxmin))
