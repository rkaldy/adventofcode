ret_a, ret_b = 0, 0

while line = gets
  a, b, c, d = line.scan(/\d+/).map{|s| s.to_i}
  ret_a += 1 if (a <= c && d <= b) || (c <= a && b <= d)
  ret_b += 1 if !(b < c || d < a)
end

puts ret_a, ret_b
