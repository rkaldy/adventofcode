require "set"

def solve(line, siglen)
  window = Array.new(siglen, ' ')
  i = 0
  line.each_char {|c| 
    window[i % siglen] = c
    if i >= siglen-1 and Set.new(window).size == siglen then
      puts i + 1
      return
    end 
    i += 1
  }
end

line = gets
solve(line, 4)
solve(line, 14)
