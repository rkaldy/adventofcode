require "json"

def compare(left, right)
  if left.class == Integer && right.class == Integer then
    return left <=> right
  elsif left.class == Array && right.class == Array then
    for i in 0...[left.size, right.size].min do
      ret = compare(left[i], right[i])
      return ret if ret != 0
    end
    return left.size <=> right.size
  elsif left.class == Integer then
    return compare([left], right)
  else
    return compare(left, [right])
  end
end

i = 0
ret_a = 0
packets = []
loop do
  i += 1
  left = JSON.parse(gets)
  right = JSON.parse(gets)
  ret_a += i if compare(left, right) == -1
  packets += [left, right]
  break if !gets
end
puts(ret_a)

packets += [[[2]], [[6]]]
packets.sort! {|left, right| compare(left, right)}
ret_b = 1
packets.each_with_index {|p, i| ret_b *= (i+1) if p == [[2]] || p == [[6]]}
puts(ret_b)
