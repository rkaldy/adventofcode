class Monkey
  attr_accessor :name, :res
  OPS = { 
    "+" => [Proc.new{|a, b| a + b}, Proc.new{|r, a| r - a}, Proc.new{|r, b| r - b}],
    "-" => [Proc.new{|a, b| a - b}, Proc.new{|r, a| a - r}, Proc.new{|r, b| r + b}],
    "*" => [Proc.new{|a, b| a * b}, Proc.new{|r, a| r / a}, Proc.new{|r, b| r / b}],
    "/" => [Proc.new{|a, b| a / b}, Proc.new{|r, a| a / r}, Proc.new{|r, b| r * b}]
}

  def initialize(line)
    if line =~ /[a-z]+: [0-9]+/ then
      @name, @num = line.scan(/([a-z]+): ([0-9]+)/)[0]
      @num = @num.to_i
    else
      @name, @left, @op_str, @right = line.scan(/([a-z]+): ([a-z0-9]+) (.) ([a-z]+)/)[0]
      @op = OPS[@op_str]
    end
    @res = nil
  end

  def compute(pack)
    if @num != nil then
      @res = @num
    else
      left = pack[@left].compute(pack)
      right = pack[@right].compute(pack)
      @res = @op[0].call(left, right)
    end
    return @res
  end

  def compute2(pack)
    if @name == "humn" then
      @res = nil
    elsif @num != nil then
      @res = @num
    else
      left = pack[@left].compute2(pack)
      right = pack[@right].compute2(pack)
      if left && right then
        @res = @op[0].call(left, right)
      end
    end
    return @res
  end

  def solve2(pack, expected = nil)
    if @name == "root" then
      if pack[@left].res then
        return pack[@right].solve2(pack, pack[@left].res)
      elsif pack[@right].res then
        return pack[@left].solve2(pack, pack[@right].res)
      else
        assert false
      end
    elsif @name == "humn" then
      return expected
    else
      if pack[@left].res then
        right_exp = @op[1].call(expected, pack[@left].res)
        return pack[@right].solve2(pack, right_exp)
      elsif pack[@right].res then
        left_exp = @op[2].call(expected, pack[@right].res)
        return pack[@left].solve2(pack, left_exp)
      else
        assert false
      end
    end
  end

  def to_s
    "#{@name}: #{@left} #{@op_str} #{@right} (=#{@res})"
  end
end

pack = {}
while line = gets do
  monkey = Monkey.new(line)
  pack[monkey.name] = monkey
end

puts(pack["root"].compute(pack))

pack.each {|name, monkey| monkey.res = nil}
pack["root"].compute2(pack)
puts(pack["root"].solve2(pack))

