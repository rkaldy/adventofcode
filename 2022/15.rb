require_relative "point"
require "set"

class Sensor < Point
  attr_accessor :dist

  def initialize(x, y, dist)
    @x, @y, @dist = x, y, dist
  end
end


def parse
  sensors = []
  beacons = Set.new
  while line = gets do
    sx, sy, bx, by = line.scan(/Sensor at x=([\d-]+), y=([\d-]+): closest beacon is at x=([\d-]+), y=([\d-]+)/)[0].map(&:to_i)
    dist = (sx - bx).abs + (sy - by).abs
    sensors.push Sensor.new(sx, sy, dist)
    beacons.add Point.new(bx, by)
  end
  return sensors, beacons
end

def intervals(y, sensors)
  row = {}
  sensors.each do |s|
    w = s.dist - (y - s.y).abs
    if w >= 0 then
      xfrom = s.x - w
      xto = s.x + w
      if row[xfrom] != nil then
        row[xfrom] += 1
      else
        row[xfrom] = 1
      end
      if row[xto] != nil then
        row[xto] -= 1
      else
        row[xto] = -1
      end
    end
  end
  return row.sort
end

def solve_a(y, sensors, beacons)
  i = 0
  ret = 0
  xlast = nil
  intervals(y, sensors).each do |x, st|
    if i == 0 then
      ret += 1
    else
      ret += x - xlast
    end
    i += st
    xlast = x
  end
  ret -= beacons.count {|b| b.y == y}
  return ret 
end

def solve_b(limit, sensors, beacons)
  for y in 0..limit do
    i = 0
    xlast = nil
    intervals(y, sensors).each do |x, st|
      if i == 0 && xlast != x - 1 && x > 0 && x <= limit then
        return (x-1) * 4000000 + y
      end
      i += st
      xlast = x
    end
  end
end

sensors, beacons = parse
puts(solve_a(2000000, sensors, beacons))
puts(solve_b(4000000, sensors, beacons))
