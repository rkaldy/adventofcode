require "set"

ORE = 0
CLAY = 1
OBSIDIAN = 2
GEODE = 3
ORE_BOTS = 4
CLAY_BOTS = 5
OBSIDIAN_BOTS = 6
GEODE_BOTS = 7

blueprints = []
while line = gets do
  ore_ore, clay_ore, obs_ore, obs_clay, geode_ore, geode_obs = line.scan(/Blueprint \d+: Each ore robot costs (\d+) ore. Each clay robot costs (\d+) ore. Each obsidian robot costs (\d+) ore and (\d+) clay. Each geode robot costs (\d+) ore and (\d+) obsidian./)[0].map(&:to_i)
  blueprints.push([[ore_ore, 0, 0], [clay_ore, 0, 0], [obs_ore, obs_clay, 0], [geode_ore, 0, geode_obs], [0, 0, 0]])
end


def add_state(states, ns)
  better = false
#  puts("#{states.inspect} <- #{ns.inspect}")
  for s in states do
    for k in 0...7 do
      if ns[k] > s[k] then
        better = true
        break
      end
    end
    return if not better
  end
  states.push(ns)
end


for i in 0...blueprints.size do
  bp = blueprints[i]
  states = Set.new
  states.add([0, 0, 0, 0, 1, 0, 0, 0])
  for t in 1..24 do
    nstates = Set.new
    for s in states do
      can_create = true
      for j in 0...bp.size do
        r = bp[j]
        if s[ORE] >= r[ORE] && s[CLAY] >= r[CLAY] && s[OBSIDIAN] >= r[OBSIDIAN] then
          ns = s.clone
          ns[ORE] += s[ORE_BOTS] - r[ORE]
          ns[CLAY] += s[CLAY_BOTS] - r[CLAY]
          ns[OBSIDIAN] += s[OBSIDIAN_BOTS] - r[OBSIDIAN]
          ns[GEODE] += s[GEODE_BOTS]
          ns[j + 4] += 1 if j != 4
          if not (j == 4 && can_create) then
            nstates.add(ns)
          end
          #add_state(nstates, ns)
        else
          can_create = false
        end
      end
    end
    states = nstates
    puts("t=#{t} states=#{states.size}")
    $stdout.flush
  end
end

