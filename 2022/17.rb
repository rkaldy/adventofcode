require_relative "point"
require "set"

class Rock
  attr_accessor :shape, :height

  def initialize(shape = nil)
    @shape = []
    return if !shape
    x, y = 0, 0
    for c in shape.chars do
      if c == '#' then
        @shape.push(Point.new(x, y)) 
      elsif c == '|' then
        y += 1
        x = -1
      end
      x += 1
    end
    @height = y + 1
  end

  def clone
    new = Rock.new
    new.height = @height
    new.shape = @shape.map {|s| s.clone}
    return new
  end

  def move(d)
    @shape.map! {|s| s += d}
  end

  def check_move(d, space)
    for s in @shape do
      t = s + d
      return false if !t.x.between?(0, 6) or t.y < 0 or space.get(t)
    end
    move(d)
    return true
  end

  def place(space)
    @shape.each {|s| space.put(s, true) }
  end

  def top
    return @shape[0].y + @height
  end

  def to_s
    @shape.join(" ")
  end
end


ROCKS = [
  Rock.new("####"),
  Rock.new(".#.|###|.#."),
  Rock.new("###|..#|..#"),
  Rock.new("#|#|#|#"),
  Rock.new("##|##")
]

DOWN = Point.new(0, -1)


def solve(jet, count)
  space = Array2d.new(6, 7, false)
  i, j, height = 0, 0, 0
  states = {}
  until i == count do
    r = ROCKS[i % ROCKS.size].clone
    r.move(Point.new(2, height + 3))
    while true do
      r.check_move(jet[j % jet.size], space)
      j += 1
      break if !r.check_move(DOWN, space)
    end
    r.place(space)
    height = r.top if r.top > height
    space.expand_y(height + 5, false)

    state_key = [i % ROCKS.size, j % jet.size, r.shape[0].x]
    state_val = [i, height]
    if states.include?(state_key)
      old_val = states[state_key]
      irep = i - old_val[0]
      hrep = height - old_val[1]
      rep = (count - 1 - i) / irep
      i += rep * irep
      states.clear
    end
    states[state_key] = state_val
    i += 1
  end
  return height + hrep * rep
end



jet = gets.chomp.chars.map {|c| Point.new((c == "<" ? -1 : 1), 0) }
puts(solve(jet, 2022))
puts(solve(jet, 1000000000000))
