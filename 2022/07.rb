def build_tree
  root = { :size => 0 }
  dir = root
  mode = nil
  while line = gets()
    line = line.split
    if line[0] == "$" then
      mode = :cmd
      if line[1] == "cd" then
        if line[2] == "/" then
          dir = root
        else
          dir = dir[line[2]]
        end
      elsif line[1] == "ls" then
        mode = :list
      end
    elsif mode == :list then
      if line[0] == "dir" then
        dir[line[1]] = { ".." => dir, :size => 0 }
      else
        filesize = line[0].to_i
        dir[:size] += filesize
        up = dir
        while (up = up[".."]) != nil do
          up[:size] += filesize
        end
      end
    end
  end
  return root
end

def run(dir, need)
  ret_a = dir[:size] <= 100000 ? dir[:size] : 0
  ret_b = dir[:size] >= need ? dir[:size] : 0
  for k, v in dir
    if k != :size && k != ".." then
      sub_a, sub_b = run(v, need) 
      ret_a += sub_a
      ret_b = sub_b if sub_b != 0 && sub_b < ret_b
    end
  end
  return ret_a, ret_b
end

root = build_tree
need = 30000000 - (70000000 - root[:size])
puts(run(root, need))
