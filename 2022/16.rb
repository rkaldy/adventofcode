class Valve
  attr_accessor :flow, :to

  def initialize(flow, to)
    @flow, @to = flow, to
  end
end

def parse
  valves = {}
  while line = gets do
    name, flow, to = line.scan(/Valve ([A-Z]+) has flow rate=(\d+); tunnels? leads? to valves? ([A-Z ,]+)/)[0]
    valves[name] = Valve.new(flow.to_i, to.split(", "))
  end
  return valves
end

def update_state(state, t, i, value)
  if t < 30 then
    state[t][i] = value if state[t][i] == nil || value > state[t][i] 
  end
end

valves = parse
state = Array.new(30) {{}}
state[0]["AA"] = 0

for t in 0...30 do
  puts("t=#{t+1}")
  puts(state[t].inspect)
  for i, p in state[t] do
    if p != nil then
      for j in valves[i].to do
        print("#{i}      -> #{j}, value=#{p}\n")
        update_state(state, t+1, j, p)
        print("#{i} open -> #{j}, value=#{p}+#{valves[i].flow*(29-t)}\n")
        update_state(state, t+2, j, p + valves[i].flow * (29-t))
      end
    end
  end
#  puts(state[t+1].inspect) if t < 29
#  puts(state[t+2].inspect) if t < 28
end
puts(state[29].max)
