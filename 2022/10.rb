require_relative "device"

dev = Device.new($stdin.readlines)
ret_a = 0
crt = Array.new(6) { "." * 40 }
dev.run { |x, cycle|
  if [20, 60, 100, 140, 180, 220].include?(cycle) then
    ret_a += cycle * x
  end
  print("#{cycle}: #{x}\n")
  col = (cycle - 1) % 40
  row = (cycle - 1) / 40
  if col >= x - 1 && col <= x + 1 then
    crt[row][col] = "#"
  end
}

puts ret_a
crt.each {|r| puts r}
