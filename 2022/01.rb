max = [0, 0, 0]
a = 0


def update_max(a, max)
    max[3] = a
    max.sort_by! { |n| -n }
    max.delete_at(3)
end

while s = gets
  if s == "\n"
    update_max(a, max)
    a = 0
  else
    a += s.chomp.to_i
  end
end
update_max(a, max)

puts max[0], max.sum
