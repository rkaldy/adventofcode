def bfs(start, finish, step, step_cond, dist)
  if finish.class != Proc then
    finish = ->(p) { p == finish }
  end
  if step_cond == nil then
    step_cond = ->(p, q) { true }
  end
  queue = [start]
  band = 0
  loop do
    band += 1
    nqueue = []
    queue.each do |p|
      step.call(p) do |q|
        if step_cond.call(p, q) then
          if band < dist.get(q) then
            dist.put(q, band)
            return band if finish(q)
            nqueue.push(q)
          end
        end
      end
    end
    queue = nqueue
  end
end

