$monkeys = []
Operators = {"*" => :mul, "+" => :add}

class Monkey
  attr_accessor :items, :inspect_count, :module

  def initialize(num, pack, stream)
    @num, @pack = num, pack
    @items = stream.gets.scan(/ *Starting items: ([\d, ]+)/)[0][0].split(", ").map(&:to_i)
    op, param = stream.gets.scan(/ *Operation: new = old (.) (\w+)/)[0]
    @op = Operators[op]
    if param == "old" then
      @op = :sqr
    else
      @param = param.to_i
    end
    @module = parse(stream, "Test: divisible by INT")
    @iftrue = parse(stream, "If true: throw to monkey INT")
    @iffalse = parse(stream, "If false: throw to monkey INT")
    @inspect_count = 0
  end

  def parse(stream, pattern)
    pattern = Regexp.new " *" + pattern.sub('INT', '(\d+)')
    match = stream.gets.scan(pattern)
    match[0][0].to_i
  end

  def inspect
    for item in @items do
      if @op == :add then
        item += @param
      elsif @op == :mul then
        item *= @param
      elsif @op == :sqr then
        item **= 2
      end
      item /= 3
      target = item % @module == 0 ? @iftrue : @iffalse
      @pack.throw(target, item)
    end
    @inspect_count += @items.size
    @items.clear()
  end

  def receive(item)
    @items.push(item)
  end

  def to_s
    "Monkey #{@num}: " + @items.join(", ")
  end
end


class MonkeyNoRelief < Monkey
  def set_modules(mods)
    @mods = mods
    @items.map! do |item|
      @mods.map {|mod| item % mod}
    end
  end

  def inspect
    for item in @items do
      if @op == :add then
        item.map! {|r| r += @param}
      elsif @op == :mul then
        item.map! {|r| r *= @param}
      elsif @op == :sqr then
        item.map! {|r| r **= 2}
      end
      item.map!.with_index {|r, i| 
        r %= @mods[i]
      }
      target = item[@num] == 0 ? @iftrue : @iffalse
      @pack.throw(target, item)
    end
    @inspect_count += @items.size
    @items.clear()
  end

  def to_s
    "Monkey #{@num}: #{@items}"
  end
end


class Pack
  attr_accessor :monkeys

  def initialize()
    @monkeys = []
  end

  def parse(stream)
    loop do
      i = stream.gets.scan(/Monkey (\d+):/)[0][0].to_i
      @monkeys[i] = MonkeyNoRelief.new(i, self, stream)
      break if !stream.gets
    end
    mods = @monkeys.map(&:module)
    @monkeys.each {|monkey| monkey.set_modules(mods)}
  end

  def round
    @monkeys.each(&:inspect)
  end

  def throw(monkey, item)
    @monkeys[monkey].receive(item)
  end

  def get_modules
  end

  def get_inspections
    return Array.new(@monkeys.size) {|i| @monkeys[i].inspect_count}
  end

  def to_s
    @monkeys.join("\n")
  end
end


pack = Pack.new
pack.parse($stdin)

for r in 1..10000 do
  pack.round 
end
ins = pack.get_inspections.sort
puts(ins[-1] * ins[-2])

