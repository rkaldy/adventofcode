stack_a = Array.new(10){[]}
stack_b = []

while ((line = gets) =~ /^ +1/) == nil
  for i in 0...stack_a.size
    c = line[i*4+1]
    stack_a[i].append(c) if c != nil && c != ' '
  end
end
gets

stack_a.each {
  |s| s.reverse!
  stack_b.append(s.clone)
}

while line = gets
  n, from, to = line.scan(/move (\d+) from (\d+) to (\d+)/)[0].map{|s| s.to_i}
  from -= 1
  to -= 1
  c = stack_a[from].pop(n).reverse()
  stack_a[to].push(*c)
  c = stack_b[from].pop(n)
  stack_b[to].push(*c)
end

stack_a.each {|s| print s.last}
puts
stack_b.each {|s| print s.last}
puts
