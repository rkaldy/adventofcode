require_relative("point")

def parse(stream)
  rocks = []
  xmin, xmax, ymax = 99999999, 0, 0
  while line = gets do
    rocks.push line.chomp.split(" -> ").map{|a| 
      x, y = a.split(",").map(&:to_i)
      xmin = x if x < xmin
      xmax = x if x > xmax
      ymax = y if y > ymax
      Point.new(x, y)
    }
  end
  ymax += 3
  xmax = [xmax, 500 + ymax].max
  xmin = [xmin, 500 - ymax].min
  board = Array2d.new(ymax, xmax - xmin, false)
  orig = Point.new(xmin, 0)

  for rock in rocks do
    for i in 1...rock.size do
      p = rock[i-1] - orig
      d = (rock[i] - rock[i-1]).sgn
      while p != rock[i] - orig do
        p += d
        board.put(p, true)
      end
    end
    board.put(rock[0] - orig, true)
  end
  for x in 0...board.width do
    board.put(Point.new(x, board.height - 1), true)
  end

  return board, Point.new(500, 0) - orig
end


STEPS = [Point.new(0, 1), Point.new(-1, 1), Point.new(1, 1)]

def fall(board, start)
  p = start
  loop do
    moved = false
    for s in STEPS do
      if !board.get(p + s) then
        p += s
        moved = true
        break
      end
    end
    return p if !moved
  end
end

board, start = parse($stdin)
count = 0
ret_a, ret_b = nil
loop do
  p = fall(board, start)
  if ret_a == nil && p.y == board.height - 2
    ret_a = count
  end
  count += 1
  board.put(p, true)
  if ret_b == nil && p.y == 0
    ret_b = count
    break
  end
end
puts ret_a, ret_b
