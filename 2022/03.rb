require "set"

reta, retb = 0, 0
elf = 0

while line = gets
  items = line.chomp.bytes.map { |c| c >= 'a'.ord ? c - 'a'.ord : c - 'A'.ord + 26 }
  
  sz2 = items.size/2
  half = Set.new(items.slice(0, sz2))
  half &= Set.new(items.slice(sz2, sz2))
  reta += half.first + 1

  if elf % 3 == 0 
    bag = Set.new(items)
  else
    bag &= Set.new(items)
    if elf % 3 == 2
      retb += bag.first + 1
    end
  end
  
  elf += 1
end

puts reta, retb
