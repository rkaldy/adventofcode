class Instruction
  attr_accessor :name, :param

  def initialize(name, param)
    @name, @param = name, param
  end

  def to_s
    "#{@name} #{@param}"
  end
end

class Device
  attr_accessor :x, :cycle
  
  @@InstDuration = { :noop => 1, :addx => 2 }

  def initialize(src)
    @x = 1
    @cycle = 0
    @code = []
    for line in src do
      inst, param = line.scan(/[a-z0-9\-]+/)
      @code << Instruction.new(inst.to_sym, param.to_i)
    end
    @ic = 0
    @todo = @@InstDuration[@code[0].name]
  end

  def run
    while true do
      @cycle += 1
      @todo -= 1
      if @todo == 0 then
        yield @x, @cycle
        execute(@code[@ic])
        @ic += 1
        break if @ic == @code.size
        @todo = @@InstDuration[@code[@ic].name]
      else
        yield @x, @cycle
      end
    end
  end

  def execute(inst)
    case inst.name
    when :addx
      @x += inst.param
    end
  end
end
