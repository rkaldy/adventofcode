require_relative "point"
require_relative "bfs"

def parse(stream)
  start, finish = nil
  board = []
  y = 1
  while line = stream.gets
    row = line.chomp.chars.map.with_index {|c, x|
      if c == 'S' then 
        start = Point.new(x + 1, y)
        1
      elsif c == 'E' then 
        finish = Point.new(x + 1, y)
        26
      else 
        c.ord - 'a'.ord + 1
      end
    }
    board[y] = [nil] + row + [nil]
    y += 1
  end
  board[0] = [nil] * board[1].size
  board[y] = [nil] * board[1].size
  return start, finish, board
end

step = Proc.new do |p| 
  DIR.each {|d| 
    yield p + d
  }
end


start, finish, board = parse($stdin)
board = Array2d.from_a(board)

puts(bfs(
  start, 
  finish, 
  step=step, 
  ->(p, q) { board.get(q) != nil && board.get(q) - board.get(p) <= 1 }, 
  Array2d.new(board.height, board.width, 99999999)
))
puts(bfs(
  finish, 
  ->(p) { board.get(p) == 1 }, 
  step=step, 
  ->(p, q) { board.get(q) != nil && board.get(q) - board.get(p) >= -1 }, 
  Array2d.new(board.height, board.width, 99999999)
))
