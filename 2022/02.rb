score_a, score_b = 0, 0
while line = gets
  a = line[0].ord - 'A'.ord
  b = line[2].ord - 'X'.ord
  score_a += (b - a + 1) % 3 * 3 + b + 1
  score_b += b * 3 + (a + b - 1) % 3 + 1
end
puts score_a, score_b
