class Point
  attr_accessor :x, :y

  def initialize(x, y)
    @x, @y = x, y
  end

  def to_s
    "(#{@x}, #{@y})"
  end

  def hash
    [@x, @y].hash
  end

  def eql?(pt)
    @x == pt.x && @y == pt.y
  end

  def clone
    Point.new(@x, @y)
  end

  def sgn
    Point.new(@x <=> 0, @y <=> 0)
  end

  def +(pt)
    Point.new(@x + pt.x, @y + pt.y)
  end

  def -(pt)
    Point.new(@x - pt.x, @y - pt.y)
  end

  def *(coef)
    Point.new(@x * coef, @y * coef)
  end

  def /(coef)
    Point.new(@x / coef, @y / coef)
  end
  
  def ==(pt)
    eql?(pt)
  end
end

DIR = [Point.new(1, 0), Point.new(0, 1), Point.new(-1, 0), Point.new(0, -1)]


class Array2d
  attr_accessor :height, :width

  def initialize(height, width, default=nil)
    @height, @width = height, width
    @arr = Array.new(height * width, default)
  end

  def self.from_a(arr)
    Array2d.new(arr.size, arr[0].size, arr.flatten)
  end

  def put(pt, value)
    @arr[pt.y * width + pt.x] = value
  end

  def get(pt)
    @arr[pt.y * width + pt.x]
  end

  def expand_y(height, default=nil)
    return if height <= @height
    @arr.append(*([default] * width * (height - @height)))
    @height = height
  end

  def to_s
    if @arr[0] === true || @arr[0] === false then
      for y in 0...@height do
        for x in 0...@width do
          print(@arr[y * width + x] ? "#" : ".")
        end
        print("\n")
      end
    else
      sz = Math.log10(@arr.max).truncate + 1
      for y in 0...@height do
        for x in 0...@width do
          n = @arr[y * width + x]
          if n == nil then
            print(" " * sz + ".")
          else
            printf(" %#{sz}i", n)
          end
        end
        print("\n")
      end
    end
  end
end
