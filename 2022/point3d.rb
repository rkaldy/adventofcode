class Point
  attr_accessor :x, :y, :z

  def initialize(x, y, z)
    @x, @y, @z = x, y, z
  end

  def to_s
    "(#{@x}, #{@y}, #{@z})"
  end

  def hash
    [@x, @y, @z].hash
  end

  def eql?(pt)
    @x == pt.x && @y == pt.y && @z == pt.z
  end

  def clone
    Point.new(@x, @y, @z)
  end

  def sgn
    Point.new(@x <=> 0, @y <=> 0, @z <=> 0)
  end

  def +(pt)
    Point.new(@x + pt.x, @y + pt.y, @z + pt.z)
  end

  def -(pt)
    Point.new(@x - pt.x, @y - pt.y, @z - pt.z)
  end

  def *(coef)
    Point.new(@x * coef, @y * coef, @z * coef)
  end

  def /(coef)
    Point.new(@x / coef, @y / coef, @z / coef)
  end
  
  def ==(pt)
    eql?(pt)
  end
end

DIR = [Point.new(1, 0, 0), Point.new(-1, 0, 0), Point.new(0, 1, 0), Point.new(0, -1, 0), Point.new(0, 0, 1), Point.new(0, 0, -1)]


class Array3d
  attr_accessor :height, :width, :depth

  def initialize(width, height, depth, default=nil)
    @height, @width, @depth = height, width, depth
    @arr = Array.new(height * width * depth, default)
  end

  def put(pt, value)
    @arr[(pt.z * height + pt.y) * width + pt.x] = value
  end

  def get(pt)
    @arr[(pt.z * height + pt.y) * width + pt.x]
  end
end
