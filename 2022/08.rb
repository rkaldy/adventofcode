require_relative "point"

wood = Array.new(100) { Array.new(100) }
size = 0
y = 0
while line = gets
  line.chomp!
  size = line.size
  for x in 0...size do
    wood[y][x] = line[x].to_i
  end
  y += 1
end

visible = Array.new(size) { Array.new(size, false) }
score = Array.new(size) { Array.new(size, 1) }

ret_a = 0
corner = [ Point.new(0, 0), Point.new(size-1, 0), Point.new(size-1, size-1), Point.new(0, size-1) ]
for d in 0...4 do
  xd = DIR[d]
  yd = DIR[(d+1)%4]
  for y in 0...size do
    h = -1
    st = Array.new(10, -1)
    for x in 0...size do
      p = corner[d] + yd*y + xd*x
      w = wood[p.y][p.x]
      #print("d=#{d} corner=#{corner[d]} y=#{y} x=#{x} p=#{p} w=#{w}\n")
      if w > h then
        if !visible[p.y][p.x] then
          visible[p.y][p.x] = true
          ret_a += 1
        end
        h = w
      end
      last = st[w..9].max
      if last == -1 then
        score[p.y][p.x] *= x
      else
        score[p.y][p.x] *= x - last
      end
      st[w] = x
    end
  end
end

ret_b = 0
for y in 0...size do
  ret_b = [ret_b, score[y].max].max
end

puts ret_a, ret_b
