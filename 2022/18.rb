require_relative "point3d"
require "set"


lava = Set.new()
while line = gets do
  x, y, z = line.split(",").map(&:to_i)
  lava.add(Point.new(x, y, z))
end

ret_a = 0
for c in lava do
  for d in DIR do
    ret_a += 1 if !lava.include?(c + d)
  end
end
puts ret_a


xmin = lava.map(&:x).min - 1
ymin = lava.map(&:y).min - 1
zmin = lava.map(&:z).min - 1
xmax = lava.map(&:x).max + 1
ymax = lava.map(&:y).max + 1
zmax = lava.map(&:z).max + 1
space = Array3d.new(xmax-xmin+1, ymax-ymin+1, zmax-zmin+1, :empty)
offset = Point.new(xmin, ymin, zmin)
for p in lava do
  space.put(p - offset, :lava)
end
ret_b = 0

queue = [Point.new(0, 0, 0)]
until queue.empty? do 
  nqueue = []
  for p in queue do
    for d in DIR do
      q = p + d
      if q.x.between?(0, xmax-xmin) && q.y.between?(0, ymax-ymin) && q.z.between?(0, zmax-zmin) then
        if space.get(q) == :lava then
          ret_b += 1
        elsif space.get(q) == :empty then
          space.put(q, :visited)
          nqueue.push(q)
        end
      end
    end
  end
  queue = nqueue
end

puts ret_b
