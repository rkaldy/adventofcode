#!/bin/bash

for f in *.rb; do
	NUM=`basename $f .rb`
	echo "== $NUM =="
	ruby $f < $NUM.in
done
