require_relative "point"
require "set"

rope = Array.new(10) { Point.new(0, 0) }
DIRMAP = { "R" => DIR[0], "L" => DIR[2], "U" => DIR[1], "D" => DIR[3] }

visited_a = Set.new
visited_b = Set.new
while line = gets do
  dir, steps = line.scan(/(.) (\d+)/)[0]
  #print("dir=#{dir} steps=#{steps}\n")
  steps.to_i.times {
    rope[0] += DIRMAP[dir]
    for k in 1..9 do
      if (rope[k-1].x - rope[k].x).abs > 1 || (rope[k-1].y - rope[k].y).abs > 1 then
        rope[k].x += (rope[k-1].x <=> rope[k].x)
        rope[k].y += (rope[k-1].y <=> rope[k].y)
      end
    end
    visited_a << rope[1].clone
    visited_b << rope[9].clone
    #print("#{rope[0]} #{rope[9]}\n")
  }
end

puts visited_a.size, visited_b.size
