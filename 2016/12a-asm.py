import sys,collections

CPY=1
INC=2
DEC=3
JNZ=4

reg = [0, 0, 1, 0]
#reg = {}
#for r in ("a", "b", "c", "d"):
#    reg[r] = 0
#reg["c"] = 1

Instr = collections.namedtuple("Instr", ["code", "arg"])

class Arg:
    def __init__(self, str):
        if str.isdigit():
            self.reg = False
            self.val = int(str)
        else:
            self.reg = True
            self.val = ord(str) - ord("a")

    def eval(self):
        if self.reg:
            return reg[self.val]
        else:
            return self.val


program = []
with open("12.in", "r") as f:
    for line in f:
        tok = line.strip().split();
        if tok[0] == "cpy":
            instr = Instr(CPY, [Arg(tok[1]), ord(tok[2]) - ord("a")])
        elif tok[0] == "inc":
            instr = Instr(INC, ord(tok[1]) - ord("a"))
        elif tok[0] == "dec":
            instr = Instr(DEC, ord(tok[1]) - ord("a"))
        elif tok[0] == "jnz":
            instr = Instr(JNZ, [Arg(tok[1]), int(tok[2])])
        else:
            print "Error:", line
            sys.exit(1)
        program.append(instr)

pc = 0
while pc < len(program):
    instr = program[pc]
    if instr.code == CPY:
        reg[instr.arg[1]] = instr.arg[0].eval()
        pc += 1
    elif instr.code == INC:
        reg[instr.arg] += 1
        pc += 1
    elif instr.code == DEC:
        reg[instr.arg] -= 1
        pc += 1
    elif instr.code == JNZ:
        if instr.arg[0].eval() != 0:
            pc += instr.arg[1]
        else:
            pc += 1

print reg[0]
