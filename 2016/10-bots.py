import sys, numpy
from collections import defaultdict

command = defaultdict(tuple)
bot = defaultdict(list)
output = defaultdict(list)
botCount = 0

with open("10.in", "r") as f:
    for line in f:
        tok = line.strip().split()
        if tok[0] == "value":
            i = int(tok[5])
            bot[i].append(int(tok[1]))
            if i > botCount - 1:
                botCount = i + 1
        elif tok[0] == "bot":
            i = int(tok[1])
            command[i] = (tok[5] == "output", int(tok[6]), tok[10] == "output", int(tok[11]))
            if i > botCount - 1:
                botCount = i + 1

ran = True
while ran:
    ran = False
    for i in range(botCount):
        if len(bot[i]) == 2 and command[i]:
            cmd = command[i]
            chips = sorted(bot[i])
            if chips[0] == 17 and chips[1] == 61:
                print "bot=", i
            if cmd[0]:
                output[cmd[1]].append(chips[0])
            else:
                bot[cmd[1]].append(chips[0])
            if cmd[2]:
                output[cmd[3]].append(chips[1])
            else:
                bot[cmd[3]].append(chips[1])
            bot[i] = []
            ran = True

print output
print numpy.prod(output[0]) * numpy.prod(output[1]) * numpy.prod(output[2])
