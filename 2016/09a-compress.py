import sys

with open("09.in", "r") as f:
    sum = 0
    for line in f:
        l = 0
        i = 0
        while i < len(line):
            if line[i] == '(':
                e = line.find( ')', i)
                (cch, rep) = line[i+1:e].split('x')
                l += int(cch) * int(rep)
                i = e + int(cch)
                for j in range(int(rep)):
                    sys.stdout.write(line[e+1:e+1+int(cch)])
            elif not line[i].isspace():
                sys.stdout.write(line[i])
                l += 1
            i += 1
        sum += l
        print "\n", l 
    print "sum=", sum
