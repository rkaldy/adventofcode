#include <algorithm>
#include <cmath>
#include <bitset>
#include <string>
#include <iostream>

using namespace std;

int main() {
	bitset<100> row, next;
	string input;
	cin >> input;
	row[0] = 0;
	int size;
	for (size = 0; size < input.size(); size++) {
		if (input[size] == '.') {
			row[size+1] = 0;
		} else if (input[size] == '^') {
			row[size+1] = 1;
		} else break;
	}
	row[size+1] = 0;

	int rule = 90;
	int count = 0;
	for (int r = 0; r < 400000; r++) {
		next[0] = next[size+1] = 0;
		for (int i = 1; i <= size; i++) {
			if (row[i] == 0) count++;
			int p = row[i-1] * 4 + row[i] * 2 + row[i+1];
			next[i] = rule & (1 << p);
		}
		row = next;
	}
	cout << count << endl;

	return 0;
}
