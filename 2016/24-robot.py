import sys
from numpy import *
from collections import defaultdict
from recordclass import recordclass

Coord = recordclass("Coord", "x y")
dirs = [Coord(0, -1),  Coord(-1, 0), Coord(0, 1), Coord(1, 0)]
WIDTH=200
HEIGHT=50
POINT_COUNT=8

maze = zeros((WIDTH, HEIGHT), int)
points = {}
graph = zeros((POINT_COUNT, POINT_COUNT), dtype=int)


def search(beg):
    dist = 0
    init = points[beg]
    used = zeros((WIDTH, HEIGHT), dtype=bool_)
    used[init.x, init.y] = True
    q = [init]
    while q:
        dist += 1
        nq = []
        for s in q:
            for d in dirs:
                ns = Coord(s.x, s.y)
                ns.x = s.x + d.x
                ns.y = s.y + d.y
                if used[ns.x, ns.y]: continue
                c = maze[ns.x, ns.y]
                if c > 0:
                    if graph[beg, c-1] == -1 or dist < graph[beg, c-1]:
                        graph[beg, c-1] = dist
                if c >= 0:
                    used[ns.x, ns.y] = True
                    nq.append(ns)
        q = nq


def perm(A):
    c = [0] * len(A)
    yield A
    i = 0
    while i < len(A):
        if c[i] < i:
            if i % 2 == 0:
                temp = A[i]
                A[i] = A[0]
                A[0] = temp
            else:
                temp = A[i]
                A[i] = A[c[i]]
                A[c[i]] = temp
            yield A
            c[i] += 1
            i = 0
        else:
            c[i] = 0
            i += 1


with open("24.in", "r") as f:
    y = 0
    for line in f:
        row = []
        for x in range(len(line.strip())):
            c = line[x]
            if c == '#':
                maze[x, y] = -1
            elif c == '.':
                maze[x, y] = -0
            else:
                points[int(c)] = Coord(x, y)
                maze[x, y] = int(c) + 1
        y += 1

    graph[:] = -1
    for i in range(POINT_COUNT):
        search(i)

    print(graph)
    minp = 99999
    for path in perm(list(range(1, POINT_COUNT))):
        d = 0
        for i in range(len(path)):
            d += graph[0 if i == 0 else path[i-1], path[i]]
        d += graph[path[-1], 0]
        if d < minp:
            minp = d
    print(minp)
