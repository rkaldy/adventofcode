import sys, numpy
from collections import defaultdict

elements = {}
floors = 4

def parseGadget(str):
    toks = str.split()
    if toks[len(toks) - 1] == "relevant.":
        return -1
    isChip = toks[len(toks) - 1] == "microchip" or toks[len(toks) - 1] == "microchip."
    el = toks[len(toks) - 2]
    if isChip:
        el = el.split("-")[0]
    if not elements.has_key(el):
        elements[el] = len(elements)
    code = elements[el]
    return 1 + code * 2 + int(isChip)


init = [0] * 20
init[0] = 1
floor = 1
for line in sys.stdin:
    for s in line.strip().split(", "):
        i = parseGadget(s)
        if (i != -1):
            init[i] = floor
    floor += 1
count = len(elements) * 2 + 1
init = tuple(init[:count])

print len(elements)
for i in init:
    print i,
print
