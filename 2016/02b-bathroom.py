import sys

dirs={"U":(0,-1),"L":(-1,0),"D":(0,1),"R":(1,0)}
m = (
(0,0,0,0,0,0,0),
(0,0,0,1,0,0,0), 
(0,0,2,3,4,0,0), 
(0,5,6,7,8,9,0),
(0,0,'A','B','C',0,0),
(0,0,0,'D',0,0,0),
(0,0,0,0,0,0,0))

x = 1
y = 3
with open("02.in","r") as f:
    for line in f:
        line = line.strip()
        for d in line:
            x += dirs[d][0]
            y += dirs[d][1]
            if m[y][x] == 0:
                x -= dirs[d][0]
                y -= dirs[d][1]
        print m[y][x],
