import sys, numpy
from collections import defaultdict
from recordclass import recordclass

forb = []
with open("20.in", "r") as f:
    for line in f:
        forb.append([int(x) for x in line.strip().split("-")])

forb.sort(key = lambda x : x[0])
minip = 0
count = 0
found = False
for f in forb:
    if f[0] > minip:
        if not found:
            print(minip)
            found = True
        count += f[0] - minip
    if (f[1] >= minip):
        minip = f[1] + 1
print(count)
count += 2**32-1 - minip
print(count)
