import sys, numpy
from collections import defaultdict

elements = {}
visited = {}
count = 0 
floors = 4

def parseGadget(str):
    toks = str.split()
    if toks[len(toks) - 1] == "relevant.":
        return -1
    isChip = toks[len(toks) - 1] == "microchip" or toks[len(toks) - 1] == "microchip."
    el = toks[len(toks) - 2]
    if isChip:
        el = el.split("-")[0]
    if not elements.has_key(el):
        elements[el] = len(elements)
    code = elements[el]
    return 1 + code * 2 + int(isChip)


def isValid(state, floor):
    hasGen = False
    for i in range((count-1) / 2):
        if state[i*2+1] == floor:
            hasGen = True
            break
    if hasGen:
        for i in range((count-1) / 2):
            if state[i*2+2] == floor and state[i*2+1] != floor:
                return False
    return True

def prints(state):
    for f in range(floors, 0, -1):
        print "F%i" % f,
        for i in range(count):
            if state[i] != f:
                print ". ",
            elif i == 0:
                print "E ",
            elif i % 2 == 1:
                print "G%i" % ((i+1)/2),
            else:
                print "M%i" % ((i+1)/2),
        print
    print


with open("11test.in", "r") as f:
    init = [0] * 20
    init[0] = 1
    floor = 1
    for line in f:
        for s in line.strip().split(", "):
            i = parseGadget(s)
            if (i != -1):
                init[i] = floor
        floor += 1
    count = len(elements) * 2 + 1
    init = tuple(init[:count])

visited[init] = True
queue = [init]
goal = tuple([floors] * count)
dist = 0
prints(init)

while True:
    newqueue = []
    dist += 1
    print "dist=%i" % dist
    for state in queue:
        floor = state[0]
        for x in range(1,count):
            if state[x] != floor:
                continue
            for y in range(x+1,count+1):
                if y != count and state[y] != floor:
                    continue
                for dir in (-1, 1):
                    if floor + dir < 1 or floor + dir > floors: 
                        continue
                    newstate = list(state)
                    newstate[0] = floor + dir
                    newstate[x] = floor + dir
                    if y != count:
                        newstate[y] = floor + dir
                    newstate = tuple(newstate)
                    if newstate == goal:
                        print "Distance", dist
                        sys.exit(0)
                    elif visited.has_key(newstate):
                        continue
                    elif isValid(newstate, floor) and isValid(newstate, floor+dir):
                        prints(newstate)
                        visited[newstate] = True
                        newqueue.append(newstate)
    queue = newqueue
