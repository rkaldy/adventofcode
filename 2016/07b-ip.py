import re


def aba(addr, hyper):
    seq = set()
    for i in range(len(addr) - 2):
        if addr[i] != addr[i+1] and addr[i] == addr[i+2]:
            if hyper:
                seq.add((addr[i+1], addr[i]))
            else:
                seq.add((addr[i], addr[i+1]))
    return seq


count = 0
with open("07.in", "r") as f:
    for line in f:
        fields = re.split("[\[\]]", line.strip())
       
        seqIp = set()
        seqHyper = set()
        for i in range(len(fields)):
            if i % 2 == 0:
                seqIp = seqIp | aba(fields[i], False)
            else:
                seqHyper = seqHyper | aba(fields[i], True)
            if seqIp & seqHyper:
                count += 1
                break

print count
