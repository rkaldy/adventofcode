import sys, numpy
from collections import defaultdict
from recordclass import recordclass


pswd = list("fbgdceah")
toks = []
with open("21.in", "r") as f:
    for line in f:
        toks.append(line.strip().split())

for tok in reversed(toks):
    if tok[0] == "swap" and tok[1] == "position":
        i = int(tok[2])
        j = int(tok[5])
        tmp = pswd[i]
        pswd[i] = pswd[j]
        pswd[j] = tmp
    elif tok[0] == "swap" and tok[1] == "letter":
        x = tok[2]
        y = tok[5]
        for i in range(len(pswd)):
            if pswd[i] == x: pswd[i] = y
            elif pswd[i] == y: pswd[i] = x
    elif tok[0] == "rotate" and tok[1] == "right":
        i = int(tok[2]) % len(pswd)
        tmp=pswd[:i]
        pswd[:len(pswd)-i] = pswd[i:len(pswd)]
        pswd[len(pswd)-i:] = tmp
    elif tok[0] == "rotate" and tok[1] == "left":
        i = int(tok[2]) % len(pswd)
        tmp=pswd[len(pswd)-i:]
        pswd[i:len(pswd)] = pswd[:len(pswd)-i]
        pswd[:i] = tmp
    elif tok[0] == "rotate" and tok[1] == "based":
        i = 0
        while i < len(pswd) and pswd[i] != tok[-1]: i += 1
        if i < len(pswd):
            if i % 2 == 1:
                i = i // 2 + 1
            elif i == 0:
                i = 1
            elif i == 2:
                i = 6
            elif i == 4:
                i = 7
            else:
                i = 0
            tmp=pswd[:i]
            pswd[:len(pswd)-i] = pswd[i:len(pswd)]
            pswd[len(pswd)-i:] = tmp
    elif tok[0] == "reverse":
        i = int(tok[2])
        j = int(tok[4])
        pswd[i:j+1] = list(reversed(pswd[i:j+1]))
    elif tok[0] == "move":
        i = int(tok[5])
        j = int(tok[2])
        tmp = pswd[i]
        if j > i:
            pswd[i:j] = pswd[i+1:j+1]
        else:
            pswd[j+1:i+1] = pswd[j:i]
        pswd[j] = tmp
    print("".join(pswd))
    
