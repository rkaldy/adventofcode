import sys
from collections import defaultdict

LEN = 8
d = [0] * LEN
for i in range(LEN):
    d[i] = defaultdict(int)

with open("06a.in", "r") as f:
    for line in f:
        for i in range(LEN):
            d[i][line[i]] += 1
print d
text = ""
for i in range(LEN):
    freq = 0
    ch = ""
    for (c, f) in d[i].items():
        if f > freq: 
            ch = c
            freq = f
    text += ch

print text
