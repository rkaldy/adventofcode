import sys

dirs=((0,-1),(-1,0),(0,1),(1,0))
ins=next(sys.stdin).strip().split(", ")

d=0
x=0
y=0
visit={}
for i in ins:
    if (i[0] == 'L'): d+=1
    if (i[0] == 'R'): d-=1
    d%=4
    step=int(i[1:])
    for j in xrange(step):
        if visit.has_key((x,y)):
            print "Found: (", x, ":", y, "), distance=", abs(x)+abs(y)
        else:
            visit[(x,y)] = 1
        x += dirs[d][0]
        y += dirs[d][1]
print "Distance=", abs(x)+abs(y)
