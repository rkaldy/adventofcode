import sys
from recordclass import recordclass

CPY=1
INC=2
DEC=3
JNZ=4
TGL=5

reg = [7, 0, 0, 0]

Instr = recordclass("Instr", ["code", "arg"])

class Arg:
    def __init__(self, str):
        if str.isdigit() or (str[0] == "-" and str[1:].isdigit()):
            self.reg = False
            self.val = int(str)
        else:
            self.reg = True
            self.val = ord(str) - ord("a")

    def eval(self):
        if self.reg:
            return reg[self.val]
        else:
            return self.val


program = []
for line in sys.stdin.readlines():
    tok = line.split()
    if tok[0] == "cpy":
        instr = Instr(CPY, [Arg(tok[1]), Arg(tok[2])])
    elif tok[0] == "inc":
        instr = Instr(INC, Arg(tok[1]))
    elif tok[0] == "dec":
        instr = Instr(DEC, Arg(tok[1]))
    elif tok[0] == "jnz":
        instr = Instr(JNZ, [Arg(tok[1]), Arg(tok[2])])
    elif tok[0] == "tgl":
        instr = Instr(TGL, Arg(tok[1]))
    else:
        prin("Error:", line)
        sys.exit(1)
    program.append(instr)

pc = 0
while pc < len(program):
    instr = program[pc]
    print(pc, instr.code, reg)
    if instr.code == CPY:
        if instr.arg[1].reg:
            reg[instr.arg[1].val] = instr.arg[0].eval()
        pc += 1
    elif instr.code == INC:
        if instr.arg.reg:
            reg[instr.arg.val] += 1
        pc += 1
    elif instr.code == DEC:
        if instr.arg.reg:
            reg[instr.arg.val] -= 1
        pc += 1
    elif instr.code == JNZ:
        if instr.arg[0].eval() != 0:
            pc += instr.arg[1].eval()
        else:
            pc += 1
    elif instr.code == TGL:
        i = pc + instr.arg.eval()
        if i >= 0 and i < len(program):
            i = program[i]
            if i.code == CPY: i.code = JNZ
            elif i.code == INC: i.code = DEC
            elif i.code == DEC: i.code = INC
            elif i.code == JNZ: i.code = CPY
            elif i.code == TGL: i.code = INC
        pc += 1

print(reg[0])
