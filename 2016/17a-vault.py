import sys, hashlib
from collections import defaultdict
from recordclass import recordclass

State = recordclass("State", ["x", "y", "path"])

DIRS = (State(0, -1, "U"), State(0, 1, "D"), State(-1, 0, "L"), State(1, 0, "R"))
PASSCODE="edjrjqaa"

q = [State(1, 1, "")]
while True:
    nq = []
    for s in q:
        doors = hashlib.md5((PASSCODE + s.path).encode("ascii")).hexdigest()
        print(s, doors[:4])
        for d in range(4):
            if doors[d] > "a":
                dir = DIRS[d]
                if s.x + dir.x >= 1 and s.x + dir.x <= 4 and s.y + dir.y >= 1 and s.y + dir.y <= 4:
                    ns = State(s.x + dir.x, s.y + dir.y, s.path + dir.path)
                    print(" -> " + str(ns))
                    if ns.x == 4 and ns.y == 4:
                        print("Found:", ns.path)
                        sys.exit(0)
                    nq.append(ns)
    q = nq
