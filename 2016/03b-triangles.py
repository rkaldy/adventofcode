with open("03.in", "r") as f:
    m = [[int(i) for i in line.strip().split()] for line in f]
    tr = 0
    for y in range(len(m)/3):
        for x in range(3):
            s = [m[y*3][x], m[y*3+1][x], m[y*3+2][x]]
            s.sort();
            if s[0] + s[1] > s[2]: tr += 1

print tr        
