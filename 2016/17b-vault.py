import sys, hashlib
from collections import defaultdict
from recordclass import recordclass

State = recordclass("State", ["x", "y", "path"])

DIRS = (State(0, -1, "U"), State(0, 1, "D"), State(-1, 0, "L"), State(1, 0, "R"))
PASSCODE="edjrjqaa"

q = [State(1, 1, "")]
longest = 0
while q:
    nq = []
    for s in q:
        doors = hashlib.md5((PASSCODE + s.path).encode("ascii")).hexdigest()
        for d in range(4):
            if doors[d] > "a":
                dir = DIRS[d]
                x = s.x + dir.x
                y = s.y + dir.y
                if x >= 1 and x <= 4 and y >= 1 and y <= 4:
                    if x == 4 and y == 4:
                        if len(s.path) + 1 > longest:
                            longest = len(s.path) + 1
                    else:
                        ns = State(x, y, s.path + dir.path)
                        nq.append(ns)
    q = nq
print(longest)
