#include <stdio.h>
#include <stdbool.h>
#include <ucwplus/ebufs.h>

#define GET(state, id) ((state >> ((id) * 2)) & 3)
#define SET(state, id, floor) state = (state & ~(3 << ((id) * 2))) | ((floor) << ((id) * 2))

#define SET_TYPE_ATOMIC unsigned int
#define SET_PREFIX(x) visited_##x
#include <ucwplus/set.h>


int count;


void print(unsigned long state) {
	for (int f = 3; f >= 0; f--) {
		printf("F%i ", f+1);
		for (int i = 0; i < count * 2 + 1; i++) {
			if (GET(state, i) != f) {
				printf(".  ");
			} else if (i == 0) {
				printf("E  ");
			} else if (i % 2 == 1) {
				printf("G%i ", (i+1)/2);
			} else {
				printf("M%i ", (i+1)/2);
			}
		}
		printf("\n");
	}
	printf("\n");
}


bool is_valid(unsigned long state, int floor) {
	bool has_gen = false;
	int i;
	for (i = 0; i < count; i++) {
		if (GET(state, i * 2 + 1) == floor) {
			has_gen = true;
			break;
		}
	}
	if (has_gen) {
		for (i = 0; i < count; i++) {
			if (GET(state, i * 2 + 2) == floor && GET(state, i * 2 + 1) != floor) {
				return false;
			}
		}
	}
	return true;
}


int main() {
	unsigned long init = 0, goal = 0;
	visited_init();

	scanf("%i", &count);
	for (int i = 0; i < count*2+1; i++) {
		int fl;
		scanf("%i", &fl);
		init += (fl - 1) << (i * 2);
		goal += 3 << (i * 2);
	}

	ints_t queue;
	ints_init(&queue);
	ints_push(&queue, init);
	visited_put(init);
	int dist = 0;

	while (true) {
		dist++;
		printf("dist=%i\n", dist);
		ints_t newqueue;
		ints_init(&newqueue);
		
		for (int i = 0; i < queue.size; i++) {
			int state = queue.ptr[i];
			int floor = GET(state, 0);
			for (int x = 1; x <= count * 2 + 1; x++) {
				if (GET(state, x) != floor) continue;
				for (int y = x + 1; y <= count * 2 + 1; y++) {
					if (y != count * 2 + 1 && GET(state, y) != floor) continue;
					for (int dir = -1; dir <= 1; dir += 2) {
						if (floor + dir < 0 || floor + dir > 3) continue;
						unsigned int newstate = state;
						SET(newstate, 0, floor + dir);
						SET(newstate, x, floor + dir);
						if (y != count * 2 + 1) {
							SET(newstate, y, floor + dir);
						}
						if (newstate == goal) {
							printf("Distance=%i\n", dist);
							return 0;
						}
						else if (visited_has(newstate)) {
							continue;
						}
						else if (is_valid(newstate, floor) && is_valid(newstate, floor + dir)) {
							visited_put(newstate);
							ints_push(&newqueue, newstate);
							//print(newstate);
						}
					}
				}
			}
		}
		ints_done(&queue);
		queue = newqueue;
	}
}
