from numpy import *

H = 6
W = 50
d = zeros((H, W), dtype=str)
d[:] = " "
lit = 0

with open("08.in", "r") as f:
    for line in f:
        tok = line.strip().split()
        if tok[0] == "rect":
            (w, h) = tok[1].split("x")
            for y in range(int(h)):
                for x in range(int(w)):
                    if d[y, x] == " ":
                        d[y, x] = "#"
                        lit += 1
        elif tok[0] == "rotate" and tok[1] == "row":
            y = int(tok[2][2:])
            r = int(tok[4])
            temp = tuple(d[y, W-r:])
            d[y, r:] = d[y, 0:W-r]
            d[y, 0:r] = temp
        elif tok[0] == "rotate" and tok[1] == "column":
            x = int(tok[2][2:])
            r = int(tok[4])
            temp = tuple(d[H-r:, x])
            d[r:, x] = d[0:H-r, x]
            d[0:r, x] = temp

for y in range(H):
    print "".join(d[y,:])
print lit
