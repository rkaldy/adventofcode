import sys


def decompress(st):
    ln = 0
    i = 0
    while i < len(st):
        if (st[i].isspace()):
            i += 1
        elif st[i] == '(':
            e = st.find( ')', i)
            (cch, rep) = st[i+1:e].split('x')
            ln += decompress(st[e+1:e+1+int(cch)]) * int(rep)
            i = e + 1 + int(cch)
        else:
            ln += 1
            i += 1
    return ln

with open("09.in", "r") as f:
    st = f.read()
ln = decompress(st)
print ln
