from sortedcontainers import *
from recordclass import recordclass

Comp = recordclass("Comp", "x y u a");

cs = []
av = SortedList()

input()
input()
for x in range(33):
    for y in range(31):
        toks = input().strip().split()
        cs.append(Comp(x, y, int(toks[2][:-1]), int(toks[3][:-1])))
        av.add(int(toks[3][:-1]))

viable = 0
for c in cs:
    if c.u != 0:
        viable += len(av) - av.bisect_left(c.u)
print(viable)
