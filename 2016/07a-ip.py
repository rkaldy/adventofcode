import re


def abba(addr):
    for i in range(len(addr) - 3):
        if addr[i] == addr[i+1]: 
            continue
        elif addr[i] == addr[i+3] and addr[i+1] == addr[i+2]:
            return True
    return False


count = 0
with open("07.in", "r") as f:
    for line in f:
        fields = re.split("[\[\]]", line.strip())
        
        ipAbba = False
        hyperAbba = False
        for i in range(len(fields)):
            if i % 2 == 1:
                if abba(fields[i]):
                    hyperAbba = True
                    break
            else:
                if abba(fields[i]):
                    ipAbba = True

        if ipAbba and not hyperAbba: 
            count += 1

print count
