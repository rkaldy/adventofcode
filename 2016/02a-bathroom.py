import sys

dirs={"U":(0,-1),"L":(-1,0),"D":(0,1),"R":(1,0)}


x = 1
y = 1
with open("02.in","r") as f:
    for line in f:
        line = line.strip()
        for d in line:
            x += dirs[d][0]
            y += dirs[d][1]
            if x < 0: x = 0
            elif x > 2: x = 2
            if y < 0: y = 0
            elif y > 2: y = 2
        print y * 3 + x + 1,
