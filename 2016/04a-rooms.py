from collections import defaultdict

with open("04.in", "r") as f:
    s = 0
    for line in f:
        (code,checksum) = line.strip().split("[")
        code = code.split("-")
        sector = code[-1]
        checksum = list(checksum)
        del code[-1]
        del checksum[-1]

        f = defaultdict(int)
        md = defaultdict(list)
        for w in code:
            for c in w:
                f[c] += 1
        for k,v in f.items():
            md[v].append(k)
        cs = []
        for k in sorted(md.keys(), reverse=True):
            cs += sorted(md[k])
        cs = cs[0:5]

        if cs == checksum:
            s += int(sector)

    print s
