import sys
from collections import defaultdict
from recordclass import recordclass

Disc = recordclass("Disc", ["size", "start"])

disc = []

with open("15.in", "r") as f:
    i = 0
    for line in f:
        tok = line.strip("\n.").split()
        disc.append(Disc(int(tok[3]), int(tok[-1])))
        
for i in range(len(disc)):
    disc[i].start = (disc[i].start + i + 1) % disc[i].size
disc.sort(key = lambda x: x.size, reverse = True)

q = 1
a = disc[0].start
for i in range(len(disc) - 1):
    q *= disc[i].size
    while a % disc[i+1].size != disc[i+1].start:
        a += q
q *= disc[-1].size

print(q - a)
