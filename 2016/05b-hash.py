import hashlib

doorid = "abbhdwsy"
pswd = "--------"
index = 0
hash = ""

while pswd.find("-") != -1:
    while True:
        index += 1
        hash = hashlib.md5(doorid + str(index)).hexdigest()
        if (hash[0:5] == "00000"): break
    pos = ord(hash[5]) - ord('0')
    if pos < 0 or pos > 7 or pswd[pos] != "-": continue
    pswd = pswd[0:pos] + hash[6] + pswd[pos+1:]
    print index, hash, pos

print pswd
