import sys, numpy
from collections import namedtuple


Pos = namedtuple("Pos", ["x", "y"])

DIRS = (Pos(1, 0), Pos(0, 1), Pos(-1, 0), Pos(0, -1))
GEN = 1358
TARGET = Pos(31, 39)

def isWall(x, y):        
    p = x*x + 3*x + 2*x*y + y + y*y + GEN
    c = 0
    while p:
        p = p & (p-1)
        c += 1
    return c % 2 == 1

queue = [Pos(1, 1)]
visited = set()
visited.add(Pos(1, 1))
dist = 0
while True:
    dist += 1
    newqueue = []
    print "dist=", dist
    for pos in queue:
        for dir in DIRS:
            newpos = Pos(pos.x + dir.x, pos.y + dir.y)
            if newpos == TARGET:
                print dist
                sys.exit(0)
            if newpos.x >= 0 and newpos.y >= 0 and not newpos in visited and not isWall(newpos.x, newpos.y):
                newqueue.append(newpos)
                visited.add(newpos)
    queue = newqueue

