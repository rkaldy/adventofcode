import sys, hashlib
from collections import defaultdict, namedtuple

salt = "ahsbgdzn"
maxIdx = -1

triples = []
quintuples = defaultdict(list)


def key(idx):
    return hashlib.md5(salt + str(idx)).hexdigest()

def check(str, idx):
    j = 0
    first = True
    for i in range(1, len(str)):
        if str[i] == str[i-1]:
            if i - j == 4:
                quintuples[str[i]].append(idx)
                return
            elif i - j == 2 and first:
                triples.append((idx, str[i]))
                first = False
        else:
            j = i


def precompute(upTo):
    global maxIdx
    for i in range(maxIdx + 1, upTo + 1):
        check(key(i), i)
    maxIdx = upTo

precompute(1000)
keyCount = 0
ti = 0
i = 0
while keyCount < 64:
    i = triples[ti][0]
    c = triples[ti][1]
    precompute(i + 1000)
    if len(quintuples[c]) != 0 and quintuples[c][len(quintuples[c]) - 1] > i:
        keyCount += 1
        print i, key(i), quintuples[c][len(quintuples[c]) - 1], key(quintuples[c][len(quintuples[c]) - 1])
    ti += 1

print "Index=", i
