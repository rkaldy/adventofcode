#include <algorithm>
#include <bitset>
#include <string>
#include <iostream>

using namespace std;

#define INPUT "10010000000110000"
#define SIZE 35651584


void print(bitset<SIZE>& d, int s) {
	for (int i = 0; i < s; i++) {
		cout << d[i];
	}
	cout << endl;
}


int main() {
	string input(INPUT);
	reverse(input.begin(), input.end());
	bitset<SIZE> d(input);
	int s = input.size();

	cout << "expand" << endl;
	while (s < SIZE) {
		d[s++] = 0;
		if (s >= SIZE) break;
		for (int i = s - 2; i >= 0; i--) {
			d[s++] = 1 - d[i];
			if (s >= SIZE) break;
		}
		cout << s << endl;
	}

	cout << "checksum" << endl;
	while (s % 2 == 0) {
		for (int i = 0; i < s/2; i++) {
			d[i] = 1 - (d[i*2] ^ d[i*2+1]);
		}
		s /= 2;
		cout << s << endl;
	}
	print(d, s);
	return 0;
}
