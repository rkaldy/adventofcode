import sys
from collections import defaultdict

LEN = 8
d = [defaultdict(int) for i in range(LEN)]

for line in open("06.in", "r"):
    for i in range(LEN):
        d[i][line[i]] += 1

text = ""
for i in range(LEN):
    freq = 999
    ch = ""
    for (c, f) in d[i].items():
        if f < freq: 
            ch = c
            freq = f
    text += ch

print text
