package main

import (
	"fmt"
	"io"
	"bufio"
	"os"
	"strings"
    "gonum.org/v1/gonum/mat"
)

const Steps = 10

func main() {
	infile := "14.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "14.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	poly, _ := reader.ReadString('\n')
	poly = strings.TrimRight(poly, "\n")
	reader.ReadString('\n')
	pair := make(map[string]string)
    used := make(map[byte]struct{})
	for {
		var from, to string
		_, err := fmt.Fscanf(reader, "%s -> %s\n", &from, &to)
		if err == io.EOF {
			break
		}
		pair[from] = to
        used[from[0]] = struct{}{}
        used[from[1]] = struct{}{}
        used[to[0]] = struct{}{}
	}
    mapping := make(map[byte]int)
    i := 0
    for c, _ := range(used) {
        mapping[c] = i
        i++
    }

    ct := len(mapping)
    A := mat.NewDense(ct*ct, ct*ct, nil)
    for from, to := range(pair) {
        f0 := mapping[from[0]]
        f1 := mapping[from[1]]
        t := mapping[to[0]]
        A.Set(f0*ct+t, f0*ct+f1, 1)
        A.Set(t*ct+f1, f0*ct+f1, 1)
    }

    v := make([]float64, ct*ct)
    for i := 0; i < len(poly)-1; i++ {
        c0 := mapping[poly[i]]
        c1 := mapping[poly[i+1]]
        v[c0*ct + c1]++
    }
    V := mat.NewDense(ct*ct, 1, v)

    A.Pow(A, 40)
    V.Mul(A, V)

    freq := make([]uint64, ct)
    for i := 0; i < ct*ct; i++ {
        freq[i / ct] += uint64(v[i])
        freq[i % ct] += uint64(v[i])
    }
    freq[mapping[poly[0]]]++
    freq[mapping[poly[len(poly)-1]]]++
    var fmin uint64 = ^uint64(0)
    var fmax uint64 = 0
    for _, f := range(freq) {
        if f < fmin {
            fmin = f
        }
        if f > fmax {
            fmax = f
        }
    }
    println(fmax/2 - fmin/2)
}
