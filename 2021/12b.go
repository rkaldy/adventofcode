package main

import (
	"fmt"
	"io"
	"bufio"
	"os"
	"strings"
	"github.com/deckarep/golang-set"
)

func dfs(start string, end string, adj *map[string][]string, visited *mapset.Set, twice string) (ret int) {
	ret = 0
	if start[0] >= 'a' && start[0] <= 'z' {
		(*visited).Add(start)
	}
	for _, next := range((*adj)[start]) {
		if next == "start" {
			continue
		} else if next == end {
			ret++
		} else if !(*visited).Contains(next) {
			ret += dfs(next, end, adj, visited, twice)
		} else if twice == "" {
			ret += dfs(next, end, adj, visited, next)
		}
	}
	if start[0] >= 'a' && start[0] <= 'z' && start != twice {
		(*visited).Remove(start)
	}
	return ret
}

func main() {
	infile := "12.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "12.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	var adj map[string][]string = make(map[string][]string)
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		fields := strings.Split(strings.TrimRight(line, "\n"), "-")
		from := fields[0]
		to := fields[1]
		adj[from] = append(adj[from], to)
		adj[to] = append(adj[to], from)
	}
	visited := mapset.NewSet();
	count := dfs("start", "end", &adj, &visited, "")
	fmt.Println(count)
}
