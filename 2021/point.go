package main

type Point struct {
	x int
	y int
}

var dirs = []Point{ Point{1, 0}, Point{0, 1}, Point{-1, 0}, Point{0, -1} }
var ddirs = []Point{ Point{1, 0}, Point{1, 1}, Point{0, 1}, Point{-1, 1}, Point{-1, 0}, Point{-1, -1}, Point{0, -1}, Point{1, -1} }

func (p *Point) Add(q Point) {
	p.x += q.x
	p.y += q.y
}

func (p Point) Dir(d int) (ret Point) {
	ret.x = p.x + dirs[d].x
	ret.y = p.y + dirs[d].y
	return
}

func (p Point) DDir(d int) (ret Point) {
	ret.x = p.x + ddirs[d].x
	ret.y = p.y + ddirs[d].y
	return
}
