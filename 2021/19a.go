package main

import (
	"fmt"
	"os"
	"sort"
)

type Edge struct {
	from int
	to int
	length int
}


func computeEdges(scanners [][][3]int) (edges [][]Edge) {
	edges = make([][]Edge, len(scanners))
	for si, bc := range(scanners) {
		edges[si] = make([]Edge, 0)
		for i := 0; i < len(bc); i++ {
			for j := i+1; j < len(bc); j++ {
				length := 0
				for k := 0; k < 3; k++ {
					d := bc[i][k] - bc[j][k]
					length += d * d
				}
				edges[si] = append(edges[si], Edge{i, j, length})
			}
		}
		sort.Slice(edges[si], func(i, j int) bool { return edges[si][i].length < edges[si][j].length })
	}
	return
}


func findOverlaps(scanners [][][3]int, edges [][]Edge) {

}


func main() {
	infile := "19.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "19.test.in"
	}
	in, _ := os.Open(infile)

	scanners := make([][][3]int, 0)
	for {
		var si int
		n, _ := fmt.Fscanf(in, "--- scanner %d ---\n", &si)
		if n != 1 {
			break
		}
		beacons := make([][3]int, 0)
		for {
			var coord [3]int
			n, _ := fmt.Fscanf(in, "%d,%d,%d\n", &coord[0], &coord[1], &coord[2])
			if n != 3 {
				break
			}
			beacons = append(beacons, coord)
		}
		scanners = append(scanners, beacons)
	}

	edges := computeEdges(scanners)
	fmt.Println(scanners)
	fmt.Println(edges)
}
