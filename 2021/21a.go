package main

import (
	"fmt"
	"os"
)

func main() {
	infile := "21.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "21.test.in"
	}
	in, _ := os.Open(infile)
	
	var i int
	var p [2]int
	for {
		var pi int
		res, _ := fmt.Fscanf(in, "Player %d starting position: %d\n", &i, &pi)
		if res == 0 {
			break
		}
		p[i-1] = pi
	}

	var score [2]int
	rolls := 0
main:
	for {
		for i = 0; i < 2; i++ {
			for j := 0; j < 3; j++ {
				rolls++
				p[i] += rolls
			}
			p[i] = (p[i] - 1) % 10 + 1
			score[i] += p[i]
			if score[i] >= 1000 {
				break main
			}
		}
	}
	println(rolls * score[1-i])
}
