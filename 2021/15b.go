package main

import (
	"io"
	"bufio"
	"os"
	"github.com/RyanCarrier/dijkstra"
)

func main() {
	infile := "15.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "15.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)
	
	b := make([]int64, 0)
	var Size int
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		for _, c := range(line) {
			if c != '\n' {
				b = append(b, int64(c - '0'))
			}
		}
		Size = len(line) - 1
	}

	g := dijkstra.NewGraph()
	for i := 0; i < Size*Size*5*5; i++ {
		g.AddVertex(i)
	}
	for y := 0; y < Size*5; y++ {
		for x := 0; x < Size*5; x++ {
            i := y * Size*5 + x
            xd := x / Size
            yd := y / Size
            xr := x % Size
            yr := y % Size
            c := (b[yr * Size + xr] + int64(xd + yd) - 1) % 9 + 1
			if x > 0 {
				g.AddArc(y * Size*5 + (x-1), i, c)
			}
			if x < Size-1 {
				g.AddArc(y * Size*5 + (x+1), i, c)
			}
			if y > 0 {
				g.AddArc((y-1) * Size*5 + x, i, c)
			}
			if y < Size-1 {
				g.AddArc((y+1) * Size*5 + x, i, c)
			}
		}
	}
	best, _ := g.Shortest(0, Size*Size*5*5-1)
	println(best.Distance)
}
