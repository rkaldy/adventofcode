package main

import (
	"io"
	"bufio"
	"strings"
	"strconv"
	"os"
)


type SNum struct {
	val int
	left *SNum
	right *SNum
	parent *SNum
}

func readRune(in *bufio.Reader, expected rune) {
	c, _, err := in.ReadRune()
	if err == io.EOF {
		panic("unexpected EOF")
	}
	if c != expected {
		panic("'" + string(expected) + "' expected")
	}
}

func read(in *bufio.Reader, parent *SNum) *SNum {
	var n SNum
	n.parent = parent
	c, _, err := in.ReadRune()
	if err == io.EOF {
		panic("unexpected EOF")
	}
	if c >= '0' && c <= '9' {
		n.val = int(c - '0')
	} else if (c == '[') {
		n.val = -1
		n.left = read(in, &n)
		readRune(in, ',')
		n.right = read(in, &n)
		readRune(in, ']')
	} else {
		panic("unexpected '" + string(c) + "'")
	}
	return &n
}

func fromString(str string) *SNum {
	return read(bufio.NewReader(strings.NewReader(str)), nil)
}


func (n *SNum) String() string {
	if n.val != -1 {
		return strconv.Itoa(n.val)
	} else {
		return "[" + n.left.String() + "," + n.right.String() + "]"
	}
}


func (n *SNum) doClone(parent *SNum) *SNum {
	var ret SNum
	if n.val == -1 {
		ret.val = -1
		ret.left = n.left.doClone(&ret)
		ret.right = n.right.doClone(&ret)
	} else {
		ret.val = n.val
	}
	ret.parent = parent
	return &ret
}

func (n *SNum) clone() *SNum {
	return n.doClone(nil)
}


func (n *SNum) explode() {
//	fmt.Println("explode", n)
	var m *SNum
	for m = n; m.parent != nil && m == m.parent.left; m = m.parent {
//		fmt.Println("left up", m)
	}
	if m.parent != nil {
		for m = m.parent.left; m.val == -1; m = m.right {
//			fmt.Println("left down", m)
		}
		(*m).val += n.left.val
	}
	for m = n; m.parent != nil && m == m.parent.right; m = m.parent {
//		fmt.Println("right up", m)
	}
	if m.parent != nil {
		for m = m.parent.right; m.val == -1; m = m.left {
//			fmt.Println("right down", m)
		}
		(*m).val += n.right.val
	}
	(*n).val = 0
	(*n).left = nil
	(*n).right = nil
}

func (n *SNum) split() {
//	oldval := n.val
	(*n).left = &SNum{n.val / 2, nil, nil, n}
	(*n).right = &SNum{(n.val + 1) / 2,  nil, nil, n}
	(*n).val = -1
//	fmt.Println("split", oldval, "to", n)
}

func (n *SNum) checkExplode(depth int) bool { 
	if n.val == -1 {
		if depth == 4 {
			n.explode()
			return true
		} else {
			done := n.left.checkExplode(depth+1)
			if !done {
				done = n.right.checkExplode(depth+1)
			}
			return done
		}
	}
	return false
}

func (n *SNum) checkSplit() bool { 
	if n.val >= 10 {
		n.split()
		return true
	} else if n.val == -1 {
		done := n.left.checkSplit()
		if !done {
			done = n.right.checkSplit()
		}
		return done
	}
	return false
}

func (n *SNum) reduce() {
	done := true
	for done {
		done = n.checkExplode(0)
		if done {
//			fmt.Println("reduce", n)
			continue
		}
		done = n.checkSplit()
//		fmt.Println("reduce", n)
	}
}


func (n *SNum) magnitude() int {
	if n.val != -1 {
		return n.val
	} else {
		return n.left.magnitude() * 3 + n.right.magnitude() * 2
	}
}


func add(a *SNum, b *SNum) *SNum {
	ret := SNum{-1, a, b, nil}
	a.parent = &ret
	b.parent = &ret
	ret.reduce()
	return &ret
}


func main() {
	infile := "18.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "18.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)
	
	nums := make([]*SNum, 0)
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		n := fromString(line)
		nums = append(nums, n)
	}
	maxmag := 0
	for i, a := range(nums) {
		for j, b := range(nums) {
			if i != j {
				sum := add(a.clone(), b.clone())
				mag := sum.magnitude()
				if mag > maxmag {
					maxmag = mag
				}
			}
		}
	}
	println(maxmag)
}
