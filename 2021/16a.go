package main

import (
	"os"
)

func (pkt *Packet) sumVer() (ret int) {
    ret += (*pkt).version
    for _, s := range((*pkt).sub) {
        ret += s.sumVer()
    }
    return
}

func main() {
    var parser *Parser
	if len(os.Args) == 2 {
        parser = NewParserStr(os.Args[1])
	} else {
        parser = NewParserFile("16.in")
    }
    pkt := parser.Parse()
    println(pkt.sumVer())
}
