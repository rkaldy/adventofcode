package main

import (
	"io"
	"bufio"
	"os"
	"github.com/gammazero/deque"
)

func main() {
	infile := "10.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "10.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	parent := map[rune]rune{ ')':'(', ']': '[', '}':'{', '>':'<' }
	score := map[rune]int{ ')': 3, ']': 57, '}': 1197, '>': 25137 }
	
	total := 0
	for {
		var stack deque.Deque
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		for _, c := range(line) {
			if c == '\n' {
				continue
			}
			open, exist := parent[c]
			if exist {
				o := stack.PopBack().(rune)
				if o != open {
					total += score[c]
				}
			} else {
				stack.PushBack(c)
			}
		}
	}
	println(total)
}
