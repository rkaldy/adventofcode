package main

import (
	"io"
	"bufio"
	"os"
	"github.com/gammazero/deque"
)

const Size = 10

func printb(b *[Size][Size]int) {
	for y := 0; y < Size; y++ {
		for x := 0; x < Size; x++ {
			if b[x][y] > 9 {
				print("*")
			} else {
				print(b[x][y])
			}
		}
		println()
	}
	println()
}


func bfs(start Point, b *[Size][Size]int, flashes *int) {
	var q deque.Deque
	b[start.x][start.y] = 0
	q.PushBack(start)
	for q.Len() != 0 {
		p := q.PopFront().(Point)
		*flashes++
		for d := 0; d < 8; d++ {
			s := p.DDir(d)
			if s.x >= 0 && s.x < Size && s.y >= 0 && s.y < Size && b[s.x][s.y] != 0 {
				b[s.x][s.y]++
				if b[s.x][s.y] > 9 {
					b[s.x][s.y] = 0
					q.PushBack(s)
				}
			}
		}
	}
}


func main() {
	infile := "11.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "11.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	var b [Size][Size]int

	y := 0
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		for x, c := range(line) {
			if c != '\n' {
				b[x][y] = int(c - '0')
			}
		}
		y++
	}

	var flashes int
	step := 0
	for flashes < 100 {
		flashes = 0
		for y := 0; y < Size; y++ {
			for x := 0; x < Size; x++ {
				b[x][y]++
			}
		}
		for y := 0; y < Size; y++ {
			for x := 0; x < Size; x++ {
				if b[x][y] > 9 {
					bfs(Point{x, y}, &b, &flashes)
				}
			}
		}
		step++
	}
	println(step)
}
