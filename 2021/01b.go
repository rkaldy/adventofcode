package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	infile := "01.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "01.test.in"
	}
	in, _ := os.Open(infile)

	var depth [3]int
	var old int
	var i, count int

	for {
		_, err := fmt.Fscanf(in, "%d\n", &depth[i % 3])
		if err == io.EOF {
			break
		}
		w := depth[0] + depth[1] + depth[2]
		if i >= 3 && w > old {
			count++
		}
		i++
		old = w
	}
	println(count)
}
