package main

import (
	"fmt"
	"os"
)

func main() {
	infile := "17.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "17.test.in"
	}
	in, _ := os.Open(infile)

	var xfrom, xto, yfrom, yto int
	var vxfrom, vxto, vyfrom, vyto int
	fmt.Fscanf(in, "target area: x=%d..%d, y=%d..%d\n", &xfrom, &xto, &yfrom, &yto)
	for vxfrom = 1; vxfrom*(vxfrom+1)/2 < xfrom; vxfrom++ {}
	vxto = xto
	vyfrom = yfrom 
	vyto = -yfrom - 1
	
	count := 0
	for ivx := vxfrom; ivx <= vxto; ivx++ {
		for ivy := vyfrom; ivy <= vyto; ivy++ {
			vx := ivx
			vy := ivy
			x := 0
			y := 0
			for {
				x += vx
				y += vy
				if vx > 0 {
					vx--
				}
				vy--
				if x >= xfrom && x <= xto && y >= yfrom && y <= yto {
					count++
					break
				}
				if x > xto || y < yfrom {
					break
				}
			}
		}
	}
	println(count)
}
