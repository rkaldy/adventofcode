package main

import (
	"fmt"
	"io"
	"bufio"
	"os"
	"strings"
)

const Steps = 10

func main() {
	infile := "14.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "14.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	poly, _ := reader.ReadString('\n')
	poly = strings.TrimRight(poly, "\n")
	reader.ReadString('\n')
	pair := make(map[string]string)
	for {
		var from, to string
		_, err := fmt.Fscanf(reader, "%s -> %s\n", &from, &to)
		if err == io.EOF {
			break
		}
		pair[from] = to
	}

	for i := 0; i < Steps; i++ {
		newpoly := ""
		for j := 0; j < len(poly)-1; j++ {
			newpoly += poly[j:j+1]
			newpoly += pair[poly[j:j+2]]
		}
		newpoly += poly[len(poly)-1:]
		poly = newpoly
	}

	freq := make(map[rune]int)
	for _, c := range(poly) {
		freq[c]++
	}
	min := 99999999
	max := 0
	for _, f := range(freq) {
		if f < min {
			min = f
		}
		if f > max {
			max = f
		}
	}
	println(max - min)
}
