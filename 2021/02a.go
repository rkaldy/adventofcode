package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	infile := "02.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "02.test.in"
	}
	in, _ := os.Open(infile)

	var dir string
	var off int
	x := 0
	y := 0
	
	for {
		_, err := fmt.Fscanf(in, "%s %d\n", &dir, &off)
		if err == io.EOF {
			break
		}
		switch (dir) {
		case "forward": x += off
		case "up": y -= off
		case "down": y += off
		}
	}
	println(x, y, x*y)
}
