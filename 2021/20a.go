package main

import (
	"fmt"
	"io"
	"bufio"
	"os"
	"strings"
	"github.com/yourbasic/bit"
)


type Board struct {
	bits bit.Set
	size int
	outer bool
}

func (b *Board) get(x, y int) bool {
	if x < 0 || x >= b.size || y < 0 || y >= b.size {
		return b.outer
	} else {
		return (*b).bits.Contains(y * b.size + x)
	}
}

func (b *Board) set(x, y int, val bool) {
	if x < 0 || x >= b.size || y < 0 || y >= b.size {
		panic("out of range")
	} else if val {
		(*b).bits.Add(y * b.size + x)
	} else {
		(*b).bits.Delete(y * b.size + x)
	}
}

func (b *Board) surrounding(x, y int) (ret int) {
	for yi := y-1; yi <= y+1; yi++ {
		for xi := x-1; xi <= x+1; xi++ {
			ret <<= 1
			if b.get(xi, yi) {
				ret |= 1
			}
		}
	}
	return
}


func read(reader *bufio.Reader) (ret Board) {
	y := 0
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		ret.size = len(line) - 1
		for x, c := range(line) {
			if c == '#' {
				ret.set(x, y, true)
			}
		}
		y++
	}
	return
}

func (b *Board) String() string {
	sb := strings.Builder{}
	for y := 0; y < b.size; y++ {
		for x := 0; x < b.size; x++ {
			if b.get(x, y) {
				sb.WriteRune('#')
			} else {
				sb.WriteRune('.')
			}
		}
		sb.WriteRune('\n')
	}
	sb.WriteString("outer: ")
	if b.outer {
		sb.WriteRune('#')
	} else {
		sb.WriteRune('.')
	}
	return sb.String()
}


func (b *Board) step(algo *bit.Set) (ret Board) {
	ret.size = b.size + 2
	if b.outer {
		ret.outer = algo.Contains(511)
	} else {
		ret.outer = algo.Contains(0)
	}
	for y := 0; y < ret.size; y++ {
		for x := 0; x < ret.size; x++ {
			s := b.surrounding(x-1, y-1)
			ret.set(x, y, algo.Contains(s))
		}
	}
	return
}

func (b *Board) count() (count int) {
	for y := 0; y < b.size; y++ {
		for x := 0; x < b.size; x++ {
			if b.get(x, y) {
				count++
			}
		}
	}
	return
}


func main() {
	infile := "20.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "20.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	var algo bit.Set
	line, _ := reader.ReadString('\n')
	for i, c := range(line) {
		if c == '#' {
			algo.Add(i)
		}
	}
	reader.ReadString('\n')
	b := read(reader)

	b = b.step(&algo)
	b = b.step(&algo)
	fmt.Println(b.count())
}
