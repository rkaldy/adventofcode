package main

import (
	"io"
	"bufio"
	"os"
	"strings"
)

const Size int = 100

func main() {
	infile := "09.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "09.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)
	
	var board [Size+2][Size+2]byte
	var x, y int
	for x = 0; x < Size+2; x++ {
		for y = 0; y < Size+2; y++ {
			board[x][y] = 10
		}
	}
	y = 1
	for {
		line, err := reader.ReadString('\n')		
		if err == io.EOF {
			break
		}
		line = strings.TrimRight(line, "\n")
		for x = 0; x < len(line); x++ {
			board[x+1][y] = line[x] - '0'
		}
		y++
	}

	risk := 0
	for x = 1; x <= Size; x++ {
		for y = 1; y <= Size; y++ {
			c := board[x][y];
			if c < board[x-1][y] && c < board[x+1][y] && c < board[x][y-1] && c < board[x][y+1] {
				risk += int(c) + 1
			}
		}
	}
	println(risk)
}
