package main

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"github.com/yourbasic/bit"
)

func main() {
	infile := "03.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "03.test.in"
	}
	in, _ := os.Open(infile)
	
	var line string
	var llen int
	var nums []int
	for {
		_, err := fmt.Fscanf(in, "%s\n", &line)
		if err == io.EOF {
			break
		}
		llen = len(line)
		num, _ := strconv.ParseInt(line, 2, 32)
		nums = append(nums, int(num))
	}

	var used [2]bit.Set
	var rating [2]int
	used[0].AddRange(0, len(nums))
	used[1].AddRange(0, len(nums))
	
	for i := llen-1; i >= 0; i-- {
		var zeroes, ones [2]int
		for j := 0; j < len(nums); j++ {
			for coef := 0; coef <= 1; coef++ {
				if used[coef].Contains(j) {
					if nums[j] & (1 << uint(i)) == 0 {
						zeroes[coef]++
					} else {
						ones[coef]++
					}
				}
			}
		}
		var bit [2]int
		if ones[0] >= zeroes[0] || ones[0] == 1 && zeroes[0] == 0 {
			bit[0] = 1
		}
		if ones[1] < zeroes[1] && ones[1] != 0 || ones[1] == 1 && zeroes[1] == 0 {
			bit[1] = 1
		}
		for coef := 0; coef <= 1; coef++ {
			rating[coef] |= bit[coef] << uint(i)
		}
		for j := 0; j < len(nums); j++ {
			for coef := 0; coef <= 1; coef++ {
				if nums[j] & (1 << uint(i)) != bit[coef] << uint(i) {
					used[coef].Delete(j)
				}
			}
		}
	}

	println(rating[0], rating[1], rating[0]*rating[1])
}
