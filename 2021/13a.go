package main

import (
	"fmt"
	"os"
)

func main() {
	infile := "13.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "13.test.in"
	}
	in, _ := os.Open(infile)
	
	dots := make(map[Point]struct{})
	for {
		var pt Point
		count, _ := fmt.Fscanf(in, "%d,%d\n", &pt.x, &pt.y)
		if count == 0 {
			break
		}
		dots[pt] = struct{}{}
	}
	for {
		var axis rune
		var f int
		count, _ := fmt.Fscanf(in, "fold along %c=%d\n", &axis, &f)
		if count == 0 {
			break
		}
		if axis == 'x' {
			for pt := range(dots) {
				if pt.x > f {
					delete(dots, pt)
					pt.x = 2*f - pt.x
					dots[pt] = struct{}{}
				}
			}
		} else if axis == 'y' {
			for pt := range(dots) {
				if pt.y > f {
					delete(dots, pt)
					pt.y = 2*f - pt.y
					dots[pt] = struct{}{}
				}
			}
		}
		break
	}
	println(len(dots))
}
