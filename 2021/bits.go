package main

import (
    "os"
    "io"
    "strings"
)

type Packet struct {
    version int
    id int
    num int;
    sub []Packet
}

type Parser struct {
    reader io.Reader
    in chan byte
    pos int
}


func (p *Parser) read() {
    buf := make([]byte, 1024)
    for {
        n, err := p.reader.Read(buf)
        if err == io.EOF {
            close(p.in)
            return
        }
        for i := 0; i < n; i++ {
            b := buf[i]
            if b >= 0x41 {
                b -= 0x41 - 10
            } else if b >= 0x30 {
                b -= 0x30
            } else {
                continue
            }
            var j byte
            for j = 0x8; j != 0; j >>= 1 {
                if b & j == 0 {
                    p.in <- 0
                } else {
                    p.in <- 1
                }
            }
        }
    }
}


func (p *Parser) bits(n int) (ret int) {
    for i := 0; i < n; i++ {
        ret <<= 1
        ret |= int(<- (*p).in)
    }
    (*p).pos += n
    return
}

func (p *Parser) parseNum() (num int) {
    for {
        n := p.bits(5)
        num <<= 4
        num |= int(n & 0xf)
        if n & 0x10 == 0 {
           break
        }
    }
    return
}

func (p *Parser) parseSubPackets() (sub []Packet) {
    lti := p.bits(1)
    if lti == 0 {
        sub = make([]Packet, 0)
        plen := p.bits(15)
        end := (*p).pos + plen
        for (*p).pos < end {
            sub = append(sub, p.parsePacket())
        }
    } else {
        sub = make([]Packet, p.bits(11))
        for i := 0; i < len(sub); i++ {
            sub[i] = p.parsePacket()
        }
    }
    return
}

func (p *Parser) parsePacket() (pkt Packet) {
    pkt.version = p.bits(3)
    pkt.id = p.bits(3)
    if pkt.id == 4 {
        pkt.num = p.parseNum()
    } else {
        pkt.sub = p.parseSubPackets()
    }
    return 
}

func (p *Parser) Parse() Packet {
    (*p).in = make(chan byte, 1024)
    go p.read()
    return p.parsePacket()
}


func NewParserFile(filename string) *Parser {
    file, err := os.Open(filename)
    parser := Parser{}
    if err == nil {
        parser.reader = file
        return &parser
    } else {
        panic(err.Error())
    }
}

func NewParserStr(str string) *Parser {
    parser := Parser{}
    parser.reader = strings.NewReader(str)
    return &parser
}


func (pkt *Packet) Compute() (ret int) {
    switch (*pkt).id {
        case 0: 
            for _, s := range((*pkt).sub) {
                ret += s.Compute()
            }
        case 1: 
            ret = 1
            for _, s := range((*pkt).sub) {
                ret *= s.Compute()
            }
        case 2:
            ret = 1 << 62
            for _, s := range((*pkt).sub) {
                v := s.Compute()
                if v < ret {
                    ret = v
                }
            }
        case 3:
            for _, s := range((*pkt).sub) {
                v := s.Compute()
                if v > ret {
                    ret = v
                }
            }
        case 4:
            ret = (*pkt).num
        case 5:
            if (*pkt).sub[0].Compute() > (*pkt).sub[1].Compute() {
                ret = 1
            }
        case 6:
            if (*pkt).sub[0].Compute() < (*pkt).sub[1].Compute() {
                ret = 1
            }
        case 7:
            if (*pkt).sub[0].Compute() == (*pkt).sub[1].Compute() {
                ret = 1
            }
        default:
            panic("Invalid packet id")
    }
    return 
}
