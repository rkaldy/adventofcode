package main

import (
//	"fmt"
	"io"
	"bufio"
	"os"
	"strings"
	"sort"
	"github.com/deckarep/golang-set"
)

func str2set(s string) (ret mapset.Set) {
	ret = mapset.NewSet()
	for i := 0; i < len(s); i++ {
		ret.Add(rune(s[i]))
	}
	return
}

func set2rune(set mapset.Set) rune {
	if set.Cardinality() != 1 {
		panic("set has not 1 element")
	}
	return set.Pop().(rune)
}

func main() {
	seg := map[string]int{
		"abcefg": 0,
		"cf": 1,
		"acdeg": 2,
		"acdfg": 3,
		"bcdf": 4,
		"abdfg": 5,
		"abdefg": 6,
		"acf": 7,
		"abcdefg": 8,
		"abcdfg": 9 }

	infile := "08.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "08.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	sum := 0
	for {
		var s2, s3, s4, s7 mapset.Set
		var s5 []mapset.Set
		var mapping = map[rune]rune{}
	
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		fd := strings.Fields(line)
		for i := 0; i < 10; i++ {
			switch len(fd[i]) {
				case 2: s2 = str2set(fd[i])
				case 3: s3 = str2set(fd[i])
				case 4: s4 = str2set(fd[i])
				case 5: s5 = append(s5, str2set(fd[i]))
				case 7: s7 = str2set(fd[i])
			}
		}
		var dg, acdeg, acdfg mapset.Set
		for i := 0; i < 3; i++ {
			if s5[i].Difference(s3).Cardinality() == 2 {
				dg = s5[i].Difference(s3)
				acdfg = s5[i]
			} else if s5[i].Intersect(s4).Cardinality() == 2 {
				acdeg = s5[i]
			}
		}
		a := s3.Difference(s2)
		b := s4.Difference(s2).Difference(dg)
		c := acdeg.Intersect(s2)
		d := s4.Difference(s2).Intersect(dg)
		e := s7.Difference(acdfg.Union(b))
		f := s2.Difference(acdeg)
		g := acdfg.Difference(a.Union(d).Union(s2))

		mapping[set2rune(a)] = 'a'
		mapping[set2rune(b)] = 'b'
		mapping[set2rune(c)] = 'c'
		mapping[set2rune(d)] = 'd'
		mapping[set2rune(e)] = 'e'
		mapping[set2rune(f)] = 'f'
		mapping[set2rune(g)] = 'g'

		num := 0
		for i := 0; i < 4; i++ {
			var s []rune
			for _, r := range fd[i+11] {
				s = append(s, mapping[r])
			}
			sort.Slice(s, func(i, j int) bool { return s[i] < s[j] })
			num *= 10
			num += seg[string(s)]
		}
		sum += num
	}
	println(sum)
}
