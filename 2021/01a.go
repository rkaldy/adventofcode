package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	infile := "01.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "01.test.in"
	}
	in, _ := os.Open(infile)

	var depth int
	var old int
	var count int

	old = -1
	for {
		_, err := fmt.Fscanf(in, "%d\n", &depth)
		if err == io.EOF {
			break
		}
		if old != -1 && depth > old {
			count++
		}
		old = depth
	}
	println(count)
}
