package main

import (
	"fmt"
	"io"
	"os"
	"gonum.org/v1/gonum/mat"
)

func main() {
	infile := "06.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "06.test.in"
	}
	in, _ := os.Open(infile)

    var i int
	var v = make([]float64, 9)
	for {
		_, err := fmt.Fscanf(in, "%d,", &i)
		if err == io.EOF {
			break
		}
		v[i]++
	}

	V := mat.NewDense(9, 1, v)
	A := mat.NewDense(9, 9, []float64{
		0,1,0,0,0,0,0,0,0,
		0,0,1,0,0,0,0,0,0,
		0,0,0,1,0,0,0,0,0,
		0,0,0,0,1,0,0,0,0,
		0,0,0,0,0,1,0,0,0,
		0,0,0,0,0,0,1,0,0,
		1,0,0,0,0,0,0,1,0,
		0,0,0,0,0,0,0,0,1,
		1,0,0,0,0,0,0,0,0 })

    A.Pow(A, 256)
    V.Mul(A, V)
	println(int64(mat.Sum(V)))
}
