package main

import (
	"io"
	"bufio"
	"os"
	"sort"
	"github.com/gammazero/deque"
)

func main() {
	infile := "10.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "10.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	parent := map[rune]rune{ ')':'(', ']': '[', '}':'{', '>':'<' }
	score := map[rune]int{ '(': 1, '[': 2, '{': 3, '<': 4 }
	
	var scores []int
Main:
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		var stack deque.Deque
		for _, c := range(line) {
			if c == '\n' {
				continue
			}
			open, exist := parent[c]
			if exist {
				o := stack.PopBack().(rune)
				if o != open {
					continue Main
				}
			} else {
				stack.PushBack(c)
			}
		}

		sc := 0
		for stack.Len() != 0 {
			o := stack.PopBack().(rune)
			sc *= 5
			sc += score[o]
		}
		scores = append(scores, sc)
	}
	sort.Ints(scores)
	println(scores[len(scores) / 2])
}
