package main

import (
	"io"
	"bufio"
	"os"
	"github.com/RyanCarrier/dijkstra"
)

func main() {
	infile := "15.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "15.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)
	
	b := make([]int64, 0)
	var Size int
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		for _, c := range(line) {
			if c != '\n' {
				b = append(b, int64(c - '0'))
			}
		}
		Size = len(line) - 1
	}

	g := dijkstra.NewGraph()
	for i, _ := range(b) {
		g.AddVertex(i)
	}
	for y := 0; y < Size; y++ {
		for x := 0; x < Size; x++ {
			i := y * Size + x
			if x > 0 {
				g.AddArc(y * Size + (x-1), i, b[i])
			}
			if x < Size-1 {
				g.AddArc(y * Size + (x+1), i, b[i])
			}
			if y > 0 {
				g.AddArc((y-1) * Size + x, i, b[i])
			}
			if y < Size-1 {
				g.AddArc((y+1) * Size + x, i, b[i])
			}
		}
	}
	best, _ := g.Shortest(0, Size*Size-1)
	println(best.Distance)
}
