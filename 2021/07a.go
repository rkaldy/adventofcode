package main

import (
	"fmt"
	"io"
	"os"
    "sort"
)

func main() {
	infile := "07.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "07.test.in"
	}
	in, _ := os.Open(infile)

    var nums []int
    var n int
	for {
		_, err := fmt.Fscanf(in, "%d,", &n)
		if err == io.EOF {
			break
		}
		nums = append(nums, n)
	}
    sort.Ints(nums)
    med := nums[len(nums) / 2]
    fuel := 0
    for _, n = range(nums) {
        if n > med {
            fuel += n - med
        } else {
            fuel += med - n
        }
    }
    println(med, fuel)
}
