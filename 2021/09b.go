package main

import (
	"fmt"
	"io"
	"bufio"
	"os"
	"strings"
	"math"
	"github.com/yourbasic/bit"
	"github.com/gammazero/deque"
)

const Size = 1000


func get(b *bit.Set, pt Point) bool {
	return b.Contains(pt.y * (Size+2) + pt.x)
}

func set(b *bit.Set, pt Point) {
	b.Add(pt.y * (Size+2) + pt.x)
}

func del(b *bit.Set, pt Point) {
	b.Delete(pt.y * (Size+2) + pt.x)
}

func printb(b *bit.Set) {
	for y := 0; y < Size+2; y++ {
		for x := 0; x < Size+2; x++ {
			if b.Contains(y*(Size+2)+x) {
				print(".")
			} else {
				print("#")
			}
		}
		println()
	}
}

func bfs(b *bit.Set, start Point) (area int) {
	var q deque.Deque
	area = 1
	del(b, start)
	q.PushBack(start)
	for q.Len() != 0 {
		var p Point = q.PopFront().(Point)
		for d := 0; d < 4; d++ {
			s := p.Dir(d)
			if get(b, s) {
				area++
				del(b, s)
				q.PushBack(s)
			}
		}
	}
	return
}

func checkArea(area int, largest []int) {
	var min int = math.MaxUint32
	var imin int 
	for i, a := range(largest) {
		if a < min {
			min = a
			imin = i
		}
	}
	if area > min {
		largest[imin] = area
	}
}

func main() {
	infile := "09.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "09.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)
	
	var b bit.Set
	var pt Point
	pt.y = 1
	for {
		line, err := reader.ReadString('\n')		
		if err == io.EOF {
			break
		}
		line = strings.TrimRight(line, "\n")
		for pt.x = 1; pt.x <= len(line); pt.x++ {
			if line[pt.x - 1] != '9' {
				set(&b, pt)
			}
		}
		pt.y++
	}

	var largest = []int{ 0, 0, 0 }
	for pt.y = 1; pt.y <= Size; pt.y++ {
		for pt.x = 1; pt.x <= Size; pt.x++ {
			if get(&b, pt) {
				area := bfs(&b, pt)
				checkArea(area, largest)
				fmt.Println(largest)
			}
		}
	}

	println(largest[0] * largest[1] * largest[2])
}
