package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	infile := "08.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "08.test.in"
	}
	in, _ := os.Open(infile)
	
	var count int 
	for {
		var d string
		var s [4]string
		_, err := fmt.Fscanf(in, "%s %s %s %s %s %s %s %s %s %s | %s %s %s %s", &d, &d, &d, &d, &d, &d, &d, &d, &d, &d, &s[0], &s[1], &s[2], &s[3])
		if err == io.EOF {
			break
		}
		for i := 0; i < 4; i++ {
			switch len(s[i]) {
				case 2, 3, 4, 7: count++
			}
		}
	}
	println(count)
}
