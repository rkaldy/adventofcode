package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	infile := "03.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "03.test.in"
	}
	in, _ := os.Open(infile)
	
	var line string
	var ones [16]int
	var i, llen uint
	count := 0
	for {
		_, err := fmt.Fscanf(in, "%s\n", &line)
		if err == io.EOF {
			break
		}
		llen = uint(len(line))
		for i = 0; i < llen; i++ {
			if line[i] == '1' {
				ones[i]++
			}
		}
		count++
	}

	gamma := 0
	for i = 0; i < llen; i++ {
		gamma <<= 1
		if ones[i]*2 > count {
			gamma |= 1
		}
	}
	epsilon := (1 << llen) - 1 - gamma

	println(gamma, epsilon, gamma*epsilon)
}
