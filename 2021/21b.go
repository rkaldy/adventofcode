package main

import (
	"fmt"
	"os"
)

const MaxScore = 21
const BoardSize = 10
type Universe [(MaxScore+1) * BoardSize][(MaxScore+1) * BoardSize]int

func main() {
	infile := "21.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "21.test.in"
	}
	in, _ := os.Open(infile)

	var u Universe
	var i1, p1, i2, p2 int
	fmt.Fscanf(in, "Player %d starting position: %d\n", &i1, &p1)
	fmt.Fscanf(in, "Player %d starting position: %d\n", &i2, &p2)
	u[p1-1][p2-1] = 1

	played := true
	i := 0
	for played {
		played = false
		var v Universe
		var p, s, np, ns [2]int
		for s[0] = 0; s[0] <= MaxScore; s[0]++ {
			for p[0] = 0; p[0] < BoardSize; p[0]++ {
				for s[1] = 0; s[1] <= MaxScore; s[1]++ {
					for p[1] = 0; p[1] < BoardSize; p[1]++ {
						uc := u[s[0]*BoardSize + p[0]][s[1]*BoardSize + p[1]]
						if uc == 0 {
							continue
						} else if s[0] == MaxScore || s[1] == MaxScore {
							v[s[0]*BoardSize + p[0]][s[1]*BoardSize + p[1]] += uc
						} else {
							for d := 0; d < 27; d++ {
								moves := d/9+1 + d/3%3+1 + d%3+1
								np[i] = (p[i] + moves) % BoardSize
								np[1-i] = p[1-i]
								ns[i] = s[i] + np[i] + 1
								if (ns[i] > MaxScore) {
									ns[i] = MaxScore
								}
								ns[1-i] = s[1-i]
								v[ns[0]*BoardSize + np[0]][ns[1]*BoardSize + np[1]] += uc
								played = true
							}
						}
					}
				}
			}
		}
/*		for s[0] = 0; s[0] <= MaxScore; s[0]++ {
			for p[0] = 0; p[0] < BoardSize; p[0]++ {
				for s[1] = 0; s[1] <= MaxScore; s[1]++ {
					for p[1] = 0; p[1] < BoardSize; p[1]++ {
						uc := u[s[0]*BoardSize+p[0]][s[1]*BoardSize+p[1]]
						if uc == 0 {
							fmt.Printf(" . ")
						} else {
							fmt.Printf("%2d ", uc)
						}
					}
					fmt.Printf(" ")
				}
				fmt.Println()
			}
			fmt.Println()
		}
		fmt.Println("----") */
		u = v
		i = 1 - i
	}

	c1 := 0
	c2 := 0
	for i := 0; i < (MaxScore+1) * BoardSize; i++ {
		for j := 0; j < (MaxScore+1) * BoardSize; j++ {
			if i / BoardSize > j / BoardSize {
				c1 += u[i][j]
			} else {
				c2 += u[i][j]
			}
		}
	}
	fmt.Println(c1, c2)
}
