package main

import (
	"fmt"
	"io"
	"os"
)

const Size = 1000

func main() {
	infile := "05.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "05.test.in"
	}
	in, _ := os.Open(infile)

	var b [Size*Size]int
	var ax, ay, bx, by, dx, dy int
	for {
		_, err := fmt.Fscanf(in, "%d,%d -> %d,%d\n", &ax, &ay, &bx, &by)
		if err == io.EOF {
			break
		}
		switch {
			case ax > bx: dx = -1
			case ax < bx: dx = 1
			default: dx = 0
		}
		switch {
			case ay > by: dy = -1
			case ay < by: dy = 1
			default: dy = 0
		}
		y := ay
		for x := ax; x != bx || y != by; x += dx {
			b[y * Size + x]++
			y += dy
		}
		b[by * Size + bx]++
	}
/*	for y := 0; y < Size; y++ {
		for x := 0; x < Size; x++ {
			if b[y*Size+x] == 0 {
				print(".")
			} else {
				print(b[y*Size+x])
			}
		}
		println()
	}*/
	var over int
	for n := 0; n < Size*Size; n++ {
		if b[n] > 1 {
			over++
		}
	}
	println(over)
}
