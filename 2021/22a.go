package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	infile := "22.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "22.test.in"
	}
	in, _ := os.Open(infile)
	
	var reactor [101][101][101]bool
	for {
		var action string
		var xf, xt, yf, yt, zf, zt int
		_, err := fmt.Fscanf(in, "%s x=%d..%d,y=%d..%d,z=%d..%d\n", &action, &xf, &xt, &yf, &yt, &zf, &zt)
		if err == io.EOF {
			break
		}
		if xf < -50 || xf > 50 || xt < -50 || xt > 50 || yf < -50 || yf > 50 || yt < -50 || yt > 50 || zf < -50 || zf > 50 || zt < -50 || zt > 50  {
			continue
		}
		val := (action == "on")
		for x := xf; x <= xt; x++ {
			for y := yf; y <= yt; y++ {
				for z := zf; z <= zt; z++ {
					reactor[x+50][y+50][z+50] = val
				}
			}
		}
	}
	count := 0
	for x := 0; x <= 100; x++ {
		for y := 0; y <= 100; y++ {
			for z := 0; z <= 100; z++ {
				if reactor[x][y][z] {
					count++
				}
			}
		}
	}
	println(count)
}
