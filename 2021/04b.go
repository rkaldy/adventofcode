package main

import (
	"os"
	"io"
	"bufio"
	"strings"
	"strconv"
)

type Mapping struct {
	b int
	x int
	y int
}

func main() {
	infile := "04.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "04.test.in"
	}
	in, _ := os.Open(infile)
	reader := bufio.NewReader(in)

	line, _ := reader.ReadString('\n')
	fields := strings.Split(strings.TrimRight(line, "\n"), ",")
	nums := make([]int, len(fields))
	for i, f := range fields {
		nums[i], _ = strconv.Atoi(f)
	}

	var mapping [100][]Mapping
	bi := 0
	for {
		_, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		for y := 0; y < 5; y++ {
			line, _ := reader.ReadString('\n')
			fields := strings.Fields(strings.TrimRight(line, "\n"))
			for x, f := range fields {
				n, _ := strconv.Atoi(f)
				mapping[n] = append(mapping[n], Mapping{bi, x, y})
			}
		}
		bi++
	}

	checked := make([][5][5]bool, bi)
	won := make([]bool, bi)
	nwon := 0
	var n, win int
Bingo:
	for _, n = range nums {
		for _, m := range mapping[n] {
			if won[m.b] {
				continue
			}
			checked[m.b][m.x][m.y] = true
			var x, y int
			for x = 0; x < 5; x++ {
				if !checked[m.b][x][m.y] {
					break
				}
			}
			for y = 0; y < 5; y++ {
				if !checked[m.b][m.x][y] {
					break
				}
			}
			if x == 5 || y == 5 {
				won[m.b] = true
				nwon++
				win = m.b
				if nwon == len(won) {
					break Bingo
				}
			}
		}
	}

	rest := 0
	for ni, mi := range mapping {
		for _, m := range mi {
			if m.b == win && !checked[m.b][m.x][m.y] {
				rest += ni
			}
		}
	}

	println(n, rest, n*rest)
}
