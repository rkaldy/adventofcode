package main

import (
	"fmt"
	"io"
	"os"
	"sort"
	"github.com/yourbasic/bit"
)

type Coord [3]int

type Cube struct {
	from Coord
	to Coord
	on bool
}

type Segment struct {
	value int
	cubeId int
    from bool
}


func main() {
	infile := "22.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "22.test.in"
	}
	in, _ := os.Open(infile)
	
    cubes := make([]Cube, 0)
    var seg [3][]Segment 
    for d := 0; d < 3; d++ {
        seg[d] = make([]Segment, 0)
    }
	for {
		var action string
		var cube Cube
		_, err := fmt.Fscanf(in, "%s x=%d..%d,y=%d..%d,z=%d..%d\n", &action, &cube.from[0], &cube.to[0], &cube.from[1], &cube.to[1], &cube.from[2], &cube.to[2])
		if err == io.EOF {
			break
		}
		cube.on = (action == "on")
        for d := 0; d < 3; d++ {
            cube.to[d]++
            seg[d] = append(seg[d], Segment{ cube.from[d], len(cubes), true })
            seg[d] = append(seg[d], Segment{ cube.to[d], len(cubes), false })
        }
		cubes = append(cubes, cube)
	}
 
    for d := 0; d < 3; d++ {
        sort.Slice(seg[d], func(i, j int) bool { return seg[d][i].value < seg[d][j].value })
        for i := 0; i < len(seg[d]); i++ {
            if seg[d][i].from {
                cubes[seg[d][i].cubeId].from[d] = i
            } else {
                cubes[seg[d][i].cubeId].to[d] = i
            }
        }
    }

    var reactor bit.Set
    sz := len(cubes) * 2
    for _, cube := range(cubes) {
        for x := cube.from[0]; x < cube.to[0]; x++ {
            for y := cube.from[1]; y < cube.to[1]; y++ {
                s := x*sz*sz + y*sz
                if cube.on {
                    reactor.AddRange(s + cube.from[2], s + cube.to[2])
                } else {
                    reactor.DeleteRange(s + cube.from[2], s + cube.to[2])
                }
            }
        }
    }

    count := 0
    var c Coord
    for c[0] = 0; c[0] < sz; c[0]++ {
        for c[1] = 0; c[1] < sz; c[1]++ {
            for c[2] = 0; c[2] < sz; c[2]++ {
                if reactor.Contains((c[0] * sz + c[1]) * sz + c[2]) {
                    volume := 1
                    for d := 0; d < 3; d++ {
                        volume *= seg[d][c[d]+1].value - seg[d][c[d]].value
                    }
                    count += volume
                }
            }
        }
    }
    fmt.Println(count)
}
