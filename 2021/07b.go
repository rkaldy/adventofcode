package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	infile := "07.in"
	if len(os.Args) == 2 && os.Args[1] == "test" {
		infile = "07.test.in"
	}
	in, _ := os.Open(infile)

    var nums []int
    var n int
    sum := 0
	for {
		_, err := fmt.Fscanf(in, "%d,", &n)
		if err == io.EOF {
			break
		}
		nums = append(nums, n)
        sum += n
	}
    println(sum, len(nums))
    mean := (sum + len(nums)/2) / len(nums)
    for m := mean-2; m <= mean+2; m++ {
        fuel := 0
        var dist int
        for _, n = range(nums) {
            if (n > m) {
                dist = n - m
            } else {
                dist = m - n
            }
            fuel += dist * (dist+1) / 2
        }
        println(m, fuel)
    }
}
