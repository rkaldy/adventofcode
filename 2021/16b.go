package main

import (
	"os"
)

func main() {
    var parser *Parser
	if len(os.Args) == 2 {
        parser = NewParserStr(os.Args[1])
	} else {
        parser = NewParserFile("16.in")
    }
    pkt := parser.Parse()
    println(pkt.Compute())
}
