#include "lib/codejam.h"
#include "lib/multicycle.h"

using namespace std;

const int lower = 156218;
const int upper = 652527;

int main() {
	int c[6];
	int ret = 0;
	MULTI_CYCLE_BEGIN(6, c[dim] = (dim == 0 ? 0 : c[dim-1])) {
		int n = 0;
		bool pass = false;
		forr(i, 6) n = n*10 + c[i];
		forr(i, 5) {
			if (c[i] == c[i+1] && (i == 4 || c[i] != c[i+2]) && (i == 0 || c[i] != c[i-1])) {
				pass = true;
				break;
			}
		}
		if (pass && n >= lower && n <= upper) {
			forr (i, 6)
			LOG(n);
			ret++;
		}
	} MULTI_CYCLE_END(c[dim] < 10, c[dim]++);

	cout << ret << endl;
	return 0;
}
