import sys
from collections import defaultdict

class Intcode:
    def __init__(self, code = None, filename=None, logEnabled = False):
        if code == None:
            self.code = Intcode.readcode(filename)
        else:
            self.code = code
        self.mode = []
        self.logEnabled = logEnabled
        self.reset()

    def readcode(filename = None):
        ist = sys.stdin if filename == None else open(filename)
        return [int(s) for s in ist.readline().strip().split(",")]

    def compute(code = None, inp = [], logEnabled=False):
        comp = Intcode(code, logEnabled)
        return comp.run(inp)

    def get(self, offset):
        m = self.mode[offset - 1]
        if m == 0:
            return self.mem[self.mem[self.pi + offset]]
        elif m == 1:
            return self.mem[self.pi + offset]
        elif m == 2:
            return self.mem[self.rb + self.mem[self.pi + offset]]
        else:
            print("ERROR: invalid mode %i, pi=%i" % (m, self.pi))

    def put(self, offset, value):
        m = self.mode[offset - 1]
        if m == 0:
            self.mem[self.mem[self.pi + offset]] = value
        elif m == 2:
            self.mem[self.rb + self.mem[self.pi + offset]] = value

    def cell(self, offset):
        m = self.mode[offset - 1]
        if m == 0:
            return self.mem[self.pi + offset]
        elif m == 2:
            return self.rb + self.mem[self.pi + offset]

    def pushInput(self, inp):
        if type(inp) == list:
            self.inp += inp
        else:
            self.inp.append(inp)

    def log(self, msg):
        if self.logEnabled:
            print(msg)

    def reset(self):
        self.mem = defaultdict(int)
        for i in range(len(self.code)):
            self.mem[i] = self.code[i]
        self.inp = []
        self.out = []
        self.pi = 0
        self.rb = 0
        self.stopped = False

    def run(self, inp):
        if self.mem:
            self.reset()
        self.inp = inp if type(inp) == list else [inp]
        self.step()
        if not self.stopped:
            print("ERROR: No more input")
            sys.exit(1)
        return self.out[0] if len(self.out) == 1 else self.out

    def runInter(self, inp = []):
        if self.stopped:
            return None
        self.pushInput(inp)
        self.step()
        ret = self.out[0] if len(self.out) == 1 else self.out
        self.out = []
        return ret

    def runAsc(self, inp = None, file = None):
        if self.stopped:
            print("stopped")
            return None
        if inp == None and file == None:
            inp = []
        else:
            if file != None:
                with open(file) as f:
                    inp = f.read().strip()
            inp = [ord(c) for c in inp] + [10]
        out = self.runInter(inp)
        return "".join([chr(i) if i < 256 else str(i) for i in out])

    def step(self):
        while True:
            p = self.mem[self.pi]
            ins = p % 100
            self.mode = [p // 100 % 10, p // 1000 % 10, p // 10000 % 10]
            self.log("\npi=%i ins=%i mode=%s" % (self.pi, ins, str(self.mode)))
        
            if ins == 1:
                self.log(":%i = %i + %i" % (self.cell(3), self.get(1), self.get(2)))
                self.put(3, self.get(1) + self.get(2))
                self.pi += 4
            
            elif ins == 2:
                self.log(":%i = %i * %i" % (self.cell(3), self.get(1), self.get(2)))
                self.put(3, self.get(1) * self.get(2))
                self.pi += 4
            
            elif ins == 3:
                if len(self.inp) == 0:
                    return
                self.log("input=%s" % str(self.inp))
                self.log(":%i = %i" % (self.cell(3), self.inp[0]))
                self.put(1, self.inp.pop(0))
                self.pi += 2
            
            elif ins == 4:
                self.out.append(self.get(1))
                self.log("output %i" % self.get(1))
                self.log("output=%s" % str(self.out))
                self.pi += 2
            
            elif ins == 5:
                self.log("if %i != 0 goto %i" % (self.get(1), self.get(2)))
                if self.get(1) != 0:
                    self.pi = self.get(2)
                else:
                    self.pi += 3
            
            elif ins == 6:
                self.log("if %i == 0 goto %i" % (self.get(1), self.get(2)))
                if self.get(1) == 0:
                    self.pi = self.get(2)
                else:
                    self.pi += 3
            
            elif ins == 7:
                self.log(":%i = %i < %i" % (self.cell(3), self.get(1), self.get(2)))
                if self.get(1) < self.get(2):
                    self.put(3, 1)
                else:
                    self.put(3, 0)
                self.pi += 4
            
            elif ins == 8:
                self.log(":%i = %i == %i" % (self.cell(3), self.get(1), self.get(2)))
                if self.get(1) == self.get(2):
                    self.put(3, 1)
                else:
                    self.put(3, 0)
                self.pi += 4

            elif ins == 9:
                self.log("rb = %i + %i" % (self.rb, self.get(1)))
                self.rb += self.get(1)
                self.pi += 2
            
            elif ins == 99:
                self.log("halt")
                self.stopped = True
                return
            
            else:
                print("ERROR: Unknown instruction %i (pi=%i code=%s)" % (self.mem[self.pi], self.pi, str(self.mem)))
                sys.exit(1)
