#include "lib/codejam.h"
#include "lib/tok.h"
#include "lib/point.h"

using namespace std;


typedef struct {
	ipoint dir;
	int dist;
} elem;

map<char, ipoint> dirs = {{'L', ipoint(-1, 0)}, {'R', ipoint(1, 0)}, {'U', ipoint(0, 1)}, {'D', ipoint(0, -1)}};


vector<elem> read() {
	string line;	
	getline(cin, line);
	vector<string> toks = tokenize(line, ",");
	vector<elem> ret(toks.size());
	for (int i = 0; i < toks.size(); i++) {
		ret[i].dir = dirs[toks[i][0]];
		ret[i].dist = atoi(toks[i].c_str() + 1);
	}
	return ret;
}


int main() {
	map<ipoint, int> wire;

	vector<elem> path = read();
	ipoint p;
	int steps = 0;
	for (elem e : path) {
		for (int i = 0; i < e.dist; i++) {
			p += e.dir;
			steps++;
			wire.emplace(p, steps);
		}
	}

	path = read();
	p = ipoint(0, 0);
	steps = 0;
	int mindist = INT_MAX;
	for (elem e : path) {
		for (int i = 0; i < e.dist; i++) {
			p += e.dir;
			steps++;
			if (has(wire, p)) {
				int dist = steps + wire[p];
				if (dist < mindist) {
					mindist = dist;
				}
			}
		}
	}

	cout << mindist << endl;
	return 0;
}
