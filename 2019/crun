#!/bin/bash

if [ -z "$1" ]; then
	echo "Usage: $0 <code> <options>"
	echo
	echo "Options:"
	echo -e "  -r\tUse real input (<code>.in) instead of test input (t.in), do not log"
	echo -e "  -q\tQuiet mode, do not log anything"
	echo -e "  -v\tVerbose mode, log everything"
	echo -e "  -p\tPaginate output with less"
	echo -e "  -c\tOnly compile, do not run"
	exit 1
fi

CODE=$1
SRC=`ls $1*.cpp`
BIN=`basename $SRC .cpp`
shift

if [ "$1" == "-r" ]; then
	shift
	OPTS="-Ofast -DNDEBUG"
	if [ "$1" == "-v" ]; then
		shift
		OPTS="$OPTS -DLOGGING"
	fi
	DATA=${CODE:0:-1}.in
else
	OPTS="-g"
	if [ "$1" == "-q" ]; then
		shift
	else
		OPTS="$OPTS -DLOGGING"
	fi
	DATA=t.in
fi

rm -f $BIN
if [ "$1" == "-c" ]; then
	shift
	g++ $OPTS $SRC -o $BIN $@
elif [ "$1" == "-p" ]; then
	shift
	g++ $OPTS $SRC -o $BIN $@ && ./$BIN < $DATA 2>&1 | less
else
	g++ $OPTS $SRC -o $BIN $@ && ./$BIN < $DATA
fi
